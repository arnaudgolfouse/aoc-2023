IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-8.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day8/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(300).

       WORKING-STORAGE SECTION.
       REPLACE ==NumberOfNodes== BY ==714==.
       77 instructions     PIC X(300) VALUE SPACES.
       77 step-index       PIC 999   USAGE COMPUTATIONAL.
       77 steps-in-a-cycle PIC 9(16) USAGE COMPUTATIONAL.
       01 nodes OCCURS NumberOfNodes TIMES INDEXED BY node-index.
           02 start-node PIC X(3).
           02 left-node  PIC X(3).
           02 right-node PIC X(3).
           *> the node reached after following the instructions once.
           02 after-one-cycle PIC 999 USAGE COMPUTATIONAL.
       77 node-index-2  PIC 999.
       77 current-node  PIC X(3).
       77 searched-node PIC X(3).
       
       *> Maps a starting node to the number of cycles necessary to reach a 'Z' node.
       *> Then from this 'Z' node, the number of cycles stays the same (property of the input data).
       01 starting-nodes-cycles PIC 9(16) OCCURS NumberOfNodes TIMES USAGE COMPUTATIONAL.
       77 starting-nodes-number PIC 999.
       77 temp                  PIC 9(16) USAGE COMPUTATIONAL.
       77 gcd                   PIC 9(16) USAGE COMPUTATIONAL.
       77 total                 PIC 9(16) USAGE COMPUTATIONAL.

       77 end-of-file PIC 9.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       *> The instructions
       READ file-fd
       MOVE current-line TO instructions
       READ file-fd *> skip newline
       READ file-fd
       
       MOVE 1 TO node-index
       PERFORM UNTIL end-of-file = 1
           MOVE current-line(1:3) TO start-node(node-index)
           MOVE current-line(8:3) TO left-node(node-index)
           MOVE current-line(13:3) TO right-node(node-index)
           ADD 1 TO node-index
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       
       CLOSE file-fd
       
       MOVE 1 TO step-index
       PERFORM UNTIL instructions(step-index:1) = " "
           ADD 1 TO step-index
       END-PERFORM
       SUBTRACT 1 FROM step-index
       MOVE step-index TO steps-in-a-cycle
       
       *> Maps a node to the node reached after one cycle
       MOVE 0 TO starting-nodes-number
       PERFORM VARYING node-index-2 FROM 1 BY 1 UNTIL node-index-2 > NumberOfNodes
           MOVE node-index-2 TO node-index
           PERFORM VARYING step-index FROM 1 BY 1 UNTIL step-index > steps-in-a-cycle
               IF instructions(step-index:1) = "L"
                   MOVE left-node(node-index) TO searched-node
               ELSE
                   MOVE right-node(node-index) TO searched-node
               END-IF
               PERFORM search-node-index
           END-PERFORM
           MOVE node-index TO after-one-cycle(node-index-2)
       END-PERFORM

       *> Maps a starting node to the number of cycles necessary to end on a 'Z'-node
       PERFORM VARYING node-index FROM 1 BY 1 UNTIL node-index > NumberOfNodes
           MOVE start-node(node-index) TO current-node
           IF current-node(3:1) = "A"
               ADD 1 TO starting-nodes-number
               MOVE 0 TO starting-nodes-cycles(starting-nodes-number)
               MOVE node-index TO node-index-2
               PERFORM UNTIL (current-node(3:1) = "Z")
                   MOVE after-one-cycle(node-index-2) TO node-index-2
                   MOVE start-node(node-index-2) TO current-node
                   ADD 1 TO starting-nodes-cycles(starting-nodes-number)
               END-PERFORM
           END-IF
       END-PERFORM

       MOVE 1 TO total
       PERFORM VARYING node-index FROM 1 BY 1 UNTIL node-index > starting-nodes-number
           *> Compute a gcd with total
           MOVE total TO gcd
           MOVE starting-nodes-cycles(node-index) TO temp
           PERFORM UNTIL gcd = temp
               IF gcd > temp
                   COMPUTE gcd = FUNCTION MOD (gcd, temp)
               ELSE
                   COMPUTE temp = FUNCTION MOD (temp, gcd)
               END-IF
               IF (gcd = 0) OR (temp = 0)
                   MOVE 1 TO gcd
                   MOVE 1 TO temp
               END-IF
           END-PERFORM
           COMPUTE total = (total * starting-nodes-cycles(node-index)) / gcd
       END-PERFORM
       
       MULTIPLY steps-in-a-cycle BY total

       DISPLAY "total = " total
       
       STOP RUN.

       search-node-index.
           MOVE 1 TO node-index
           PERFORM UNTIL start-node(node-index) = searched-node
               ADD 1 TO node-index
           END-PERFORM
           .
END PROGRAM AOC-DAY-8.
