
IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-6.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day6/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line    PIC X(40).

       WORKING-STORAGE SECTION.
       77 parse-position  PIC 99.
       77 char-as-int     PIC 9.
       77 parsed-number   PIC 9(4) USAGE COMPUTATIONAL.
       77 input-times     PIC 9(4) USAGE COMPUTATIONAL OCCURS 4 TIMES INDEXED BY time-index.
       77 input-distances PIC 9(4) USAGE COMPUTATIONAL OCCURS 4 TIMES INDEXED BY distance-index.
       77 race-time       PIC 9(8) USAGE COMPUTATIONAL.
       77 race-distance   PIC 9(8) USAGE COMPUTATIONAL.
       77 discriminant    PIC 9(8) USAGE COMPUTATIONAL.
       77 first-root      PIC 9(4)V9(4) USAGE COMPUTATIONAL.
       77 second-root     PIC 9(4)V9(4) USAGE COMPUTATIONAL.
       77 combinations    PIC 9(4) USAGE COMPUTATIONAL.
       77 result          PIC 9(8) USAGE COMPUTATIONAL.

PROCEDURE DIVISION.
       PERFORM read-input
       
       MOVE 1 TO time-index
       MOVE 1 TO result
       PERFORM 4 TIMES
           MOVE input-times(time-index) TO race-time
           MOVE input-distances(time-index) TO race-distance
           COMPUTE discriminant = race-time * race-time - 4 * race-distance
           COMPUTE first-root   = (race-time - discriminant ** 0.5) / 2
           COMPUTE second-root  = (race-time + discriminant ** 0.5) / 2
           *> Technically wrong, but works here.
           *> we need ⌈r1⌉ - ⌊r2⌋ - 1
           COMPUTE combinations = (FUNCTION INTEGER(first-root)) - (FUNCTION INTEGER(second-root))

           DISPLAY combinations " combinations"
           MULTIPLY combinations BY result

           ADD 1 TO time-index
       END-PERFORM

       DISPLAY "result = " result

       STOP RUN.
       
       read-input.
           OPEN INPUT file-fd
           READ file-fd

           MOVE 11 TO parse-position
           MOVE 1 TO time-index
           PERFORM 4 TIMES
               PERFORM UNTIL NOT (current-line(parse-position:1) = " ")
                   ADD 1 TO parse-position
               END-PERFORM
               PERFORM parse-number
               MOVE parsed-number TO input-times(time-index)
               ADD 1 TO time-index
           END-PERFORM

           READ file-fd

           MOVE 11 TO parse-position
           MOVE 1 TO distance-index
           PERFORM 4 TIMES
               PERFORM UNTIL NOT (current-line(parse-position:1) = " ")
                   ADD 1 TO parse-position
               END-PERFORM
               PERFORM parse-number
               MOVE parsed-number TO input-distances(distance-index)
               ADD 1 TO distance-index
           END-PERFORM

           CLOSE file-fd
           .
       
       *> Parse a number in the current line, and put the result in `parsed-number`.
       *> After this function, `parse-position` will be right after the parsed number.
       parse-number.
           MOVE 0 TO parsed-number
           MOVE current-line(parse-position:1) TO char-as-int
           PERFORM UNTIL (NOT (current-line(parse-position:1) = "0")) AND (char-as-int = 0)
               MULTIPLY 10 BY parsed-number
               ADD char-as-int TO parsed-number
               ADD 1 TO parse-position
               MOVE current-line(parse-position:1) TO char-as-int
           END-PERFORM
           .
END PROGRAM AOC-DAY-6.
