IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-10.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day10/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(140).

       WORKING-STORAGE SECTION.
       REPLACE ==LineLength== BY ==140==.
       77 pipes PIC X(LineLength) OCCURS LineLength TIMES.
       77 is-part-of-loop PIC X(LineLength) OCCURS LineLength TIMES.
       77 pipe-line      USAGE INDEX.
       77 pipe-col       USAGE INDEX.
       77 start-line     USAGE INDEX.
       77 start-col      USAGE INDEX.
       77 enter-loop     PIC X.
       01 comes-from     PIC X.
       77 result         PIC 9(16) USAGE COMPUTATIONAL.
       77 is-inside-loop PIC 9 USAGE COMPUTATIONAL.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       PERFORM VARYING pipe-line FROM 1 BY 1 UNTIL pipe-line > LineLength
           READ file-fd
           MOVE current-line TO pipes(pipe-line)
       END-PERFORM
       CLOSE file-fd

       PERFORM VARYING pipe-line FROM 1 BY 1 UNTIL pipe-line > LineLength
           PERFORM VARYING pipe-col FROM 1 BY 1 UNTIL pipe-col > LineLength
               MOVE "N" TO is-part-of-loop(pipe-line) (pipe-col:1)
               IF pipes(pipe-line) (pipe-col:1) = "S"
                   MOVE pipe-line TO start-line
                   MOVE pipe-col TO start-col
               END-IF
           END-PERFORM
       END-PERFORM

       PERFORM find-start-position
       
       MOVE "Y" TO is-part-of-loop(start-line) (start-col:1)
       PERFORM UNTIL pipes(pipe-line) (pipe-col:1) = "S"
           MOVE "Y" TO is-part-of-loop(pipe-line) (pipe-col:1)
           PERFORM find-next-position
       END-PERFORM

       MOVE "|" TO pipes(start-line) (start-col:1)
       
       MOVE 0 TO result
       PERFORM VARYING pipe-line FROM 1 BY 1 UNTIL pipe-line > LineLength
           MOVE 0 TO is-inside-loop
           MOVE " " TO enter-loop
           PERFORM VARYING pipe-col FROM 1 BY 1 UNTIL pipe-col > LineLength
               IF is-part-of-loop(pipe-line) (pipe-col:1) = "Y"
                   IF enter-loop = " "
                       MOVE pipes(pipe-line) (pipe-col:1) TO enter-loop
                   ELSE
                       IF enter-loop = "F"
                           IF pipes(pipe-line) (pipe-col:1) = "J"
                               COMPUTE is-inside-loop = 1 - is-inside-loop
                               MOVE " " TO enter-loop
                           END-IF
                           IF pipes(pipe-line) (pipe-col:1) = "7"
                               MOVE " " TO enter-loop
                           END-IF
                       END-IF
                       IF enter-loop = "L"
                           IF pipes(pipe-line) (pipe-col:1) = "7"
                               COMPUTE is-inside-loop = 1 - is-inside-loop
                               MOVE " " TO enter-loop
                           END-IF
                           IF pipes(pipe-line) (pipe-col:1) = "J"
                               MOVE " " TO enter-loop
                           END-IF
                       END-IF
                   END-IF
                   IF (pipes(pipe-line) (pipe-col:1) = "|")
                       COMPUTE is-inside-loop = 1 - is-inside-loop
                       MOVE " " TO enter-loop
                   END-IF
               ELSE
                   IF is-inside-loop = 1
                       ADD 1 TO result
                   END-IF
               END-IF
           END-PERFORM
       END-PERFORM

       DISPLAY result
       
       STOP RUN.

       find-start-position.
           MOVE start-col TO pipe-col
           COMPUTE pipe-line = start-line - 1
           IF (pipes(pipe-line) (pipe-col:1) = "|") OR (pipes(pipe-line) (pipe-col:1) = "J") OR (pipes(pipe-line) (pipe-col:1) = "L")
               MOVE "t" TO comes-from
           END-IF
           COMPUTE pipe-line = start-line + 1
           IF (pipes(pipe-line) (pipe-col:1) = "|") OR (pipes(pipe-line) (pipe-col:1) = "F") OR (pipes(pipe-line) (pipe-col:1) = "7")
               MOVE "b" TO comes-from
           END-IF
           MOVE start-line TO pipe-line
           COMPUTE pipe-col = start-col - 1
           IF (pipes(pipe-line) (pipe-col:1) = "-") OR (pipes(pipe-line) (pipe-col:1) = "F") OR (pipes(pipe-line) (pipe-col:1) = "L")
               MOVE "r" TO comes-from
           END-IF
           COMPUTE pipe-col = start-col - 1
           IF (pipes(pipe-line) (pipe-col:1) = "-") OR (pipes(pipe-line) (pipe-col:1) = "J") OR (pipes(pipe-line) (pipe-col:1) = "7")
               MOVE "l" TO comes-from
           END-IF
           
           MOVE start-col TO pipe-col
           EVALUATE comes-from
               WHEN "l" COMPUTE pipe-col  = start-col  + 1
               WHEN "r" COMPUTE pipe-col  = start-col  - 1
               WHEN "t" COMPUTE pipe-line = start-line + 1
               WHEN "b" COMPUTE pipe-line = start-line - 1
           END-EVALUATE
           .
       
       find-next-position.
           EVALUATE pipes(pipe-line) (pipe-col:1)
               WHEN "-" IF comes-from = "l"
                       ADD 1 TO pipe-col
                   ELSE
                       SUBTRACT 1 FROM pipe-col
                   END-IF
               WHEN "|" IF comes-from = "t"
                       ADD 1 TO pipe-line
                   ELSE
                       SUBTRACT 1 FROM pipe-line
                   END-IF
               WHEN "L" IF comes-from = "t"
                       ADD 1 TO pipe-col
                       MOVE "l" TO comes-from
                   ELSE
                       SUBTRACT 1 FROM pipe-line
                       MOVE "b" TO comes-from
                   END-IF
               WHEN "J" IF comes-from = "t"
                       SUBTRACT 1 FROM pipe-col
                       MOVE "r" TO comes-from
                   ELSE
                       SUBTRACT 1 FROM pipe-line
                       MOVE "b" TO comes-from
                   END-IF
               WHEN "7" IF comes-from = "b"
                       SUBTRACT 1 FROM pipe-col
                       MOVE "r" TO comes-from
                   ELSE
                       ADD 1 TO pipe-line
                       MOVE "t" TO comes-from
                   END-IF
               WHEN "F" IF comes-from = "b"
                       ADD 1 TO pipe-col
                       MOVE "l" TO comes-from
                   ELSE
                       ADD 1 TO pipe-line
                       MOVE "t" TO comes-from
                   END-IF
           .
END PROGRAM AOC-DAY-10.
