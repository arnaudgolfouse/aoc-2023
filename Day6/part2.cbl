
IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-6.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day6/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line    PIC X(40).

       WORKING-STORAGE SECTION.
       77 parse-position  PIC 99.
       77 parsed-number   PIC 9(16) USAGE COMPUTATIONAL.
       77 char-as-int     PIC 9.
       77 race-time       PIC 9(16) USAGE COMPUTATIONAL.
       77 race-distance   PIC 9(16) USAGE COMPUTATIONAL.
       77 discriminant    PIC 9(16) USAGE COMPUTATIONAL.
       77 first-root      PIC 9(12)V9(4) USAGE COMPUTATIONAL.
       77 second-root     PIC 9(12)V9(4) USAGE COMPUTATIONAL.
       77 result          PIC 9(16) USAGE COMPUTATIONAL.
       77 display-result  PIC Z(15)9.

PROCEDURE DIVISION.
       PERFORM read-input
       
       COMPUTE discriminant = race-time * race-time - 4 * race-distance
       COMPUTE first-root   = (race-time - discriminant ** 0.5) / 2
       COMPUTE second-root  = (race-time + discriminant ** 0.5) / 2
       *> Technically wrong, but works here.
       *> we need ⌈r1⌉ - ⌊r2⌋ - 1
       COMPUTE result = (FUNCTION INTEGER(first-root)) - (FUNCTION INTEGER(second-root))
       
       MOVE result TO display-result
       DISPLAY "result = " display-result

       STOP RUN.
       
       read-input.
           OPEN INPUT file-fd

           READ file-fd
           MOVE 11 TO parse-position
           PERFORM parse-number
           MOVE parsed-number TO race-time

           READ file-fd
           MOVE 11 TO parse-position
           PERFORM parse-number
           MOVE parsed-number TO race-distance

           CLOSE file-fd
           .
       
       parse-number.
           MOVE 0 TO parsed-number
           PERFORM 4 TIMES
               PERFORM UNTIL NOT (current-line(parse-position:1) = " ")
                   ADD 1 TO parse-position
               END-PERFORM
               MOVE current-line(parse-position:1) TO char-as-int
               PERFORM UNTIL (NOT (current-line(parse-position:1) = "0")) AND (char-as-int = 0)
                   MULTIPLY 10 BY parsed-number
                   ADD char-as-int TO parsed-number
                   ADD 1 TO parse-position
                   MOVE current-line(parse-position:1) TO char-as-int
               END-PERFORM
           END-PERFORM
           .
END PROGRAM AOC-DAY-6.
