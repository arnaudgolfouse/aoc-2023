IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-19.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day19/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(50).

       WORKING-STORAGE SECTION.
       77 parse-index INDEX.
       77 char-as-int PIC 9.
       77 end-of-file PIC 9 VALUE 0.

       *> Zero-terminated (wf-name is "    ")
       01 workflows OCCURS 1000 TIMES INDEXED BY workflow-index.
           02 wf-name PIC X(3) VALUE "   ".
           *> Zero-terminated (category is " ")
           02 rules OCCURS 5 TIMES INDEXED BY rule-index.
               03 category PIC X VALUE " ".
               03 is-greater PIC 9.
               03 threshold PIC 9(4) COMP VALUE 0.
               03 dest PIC X(3) VALUE "   ".
           02 wf-default PIC X(3) VALUE "   ".
       
       *> x m a s
       01 part OCCURS 4 TIMES INDEXED BY part-category-index.
           02 part-name PIC X.
           02 part-number PIC 9(4) COMP VALUE 0.
       
       77 start-index INDEX.
       77 current-workflow-name PIC X(4).
       77 result PIC 9(10) COMP VALUE 0.
       
       77 temp-index INDEX.
       77 temp-bool PIC 9.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
       
       MOVE 1 TO workflow-index
       PERFORM UNTIL end-of-file = 1
           IF current-line(1:1) = " "
               MOVE 1 TO end-of-file
           ELSE
               PERFORM parse-workflow
               ADD 1 TO workflow-index
           END-IF
           READ file-fd
       END-PERFORM
       MOVE 0 TO end-of-file
       PERFORM UNTIL end-of-file = 1
           PERFORM parse-part
           PERFORM is-accepted
           IF current-workflow-name = "A"
               PERFORM VARYING part-category-index FROM 1 BY 1 UNTIL part-category-index > 4
                   ADD part-number(part-category-index) TO result
               END-PERFORM
           END-IF
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM

       CLOSE file-fd

       DISPLAY "result = " result
       
       STOP RUN.

       parse-workflow.
           MOVE 1 TO parse-index
           PERFORM UNTIL current-line(parse-index:1) = "{"
               MOVE current-line(parse-index:1) TO wf-name(workflow-index) (parse-index:1)
               ADD 1 TO parse-index
           END-PERFORM
           IF wf-name(workflow-index) = "in"
               MOVE workflow-index TO start-index
           END-IF
           MOVE 1 TO rule-index
           PERFORM UNTIL current-line(parse-index:1) = "}"
               ADD 1 TO parse-index
               MOVE current-line(parse-index:1) TO category(workflow-index,rule-index)
               ADD 1 TO parse-index
               IF (current-line(parse-index:1) = "<") OR (current-line(parse-index:1) = ">")
                   IF current-line(parse-index:1) = "<"
                       MOVE 0 TO is-greater(workflow-index,rule-index)
                   ELSE
                       MOVE 1 TO is-greater(workflow-index,rule-index)
                   END-IF
                   ADD 1 TO parse-index
                   PERFORM UNTIL current-line(parse-index:1) = ":"
                       MOVE current-line(parse-index:1) TO char-as-int
                       MULTIPLY 10 BY threshold(workflow-index,rule-index)
                       ADD char-as-int TO threshold(workflow-index,rule-index)
                       ADD 1 TO parse-index
                   END-PERFORM
                   ADD 1 TO parse-index
                   MOVE 1 TO temp-index
                   PERFORM UNTIL current-line(parse-index:1) = ","
                       MOVE current-line(parse-index:1) TO dest(workflow-index,rule-index) (temp-index:1)
                       ADD 1 TO parse-index
                       ADD 1 TO temp-index
                   END-PERFORM
                   ADD 1 TO rule-index
               ELSE
                   MOVE category(workflow-index,rule-index) TO wf-default(workflow-index) (1:1)
                   MOVE " " TO category(workflow-index,rule-index)
                   MOVE 2 TO temp-index
                   PERFORM UNTIL current-line(parse-index:1) = "}"
                       MOVE current-line(parse-index:1) TO wf-default(workflow-index) (temp-index:1)
                       ADD 1 TO parse-index
                       ADD 1 TO temp-index
                   END-PERFORM
               END-IF
           END-PERFORM
           .
       
       parse-part.
           *> Skip "{"
           MOVE 2 TO parse-index
           MOVE 1 TO part-category-index
           PERFORM 4 TIMES
               MOVE current-line(parse-index:1) TO part-name(part-category-index)
               MOVE 0 TO part-number(part-category-index)
               ADD 2 TO parse-index
               PERFORM UNTIL (current-line(parse-index:1) = ",") OR (current-line(parse-index:1) = "}")
                   MOVE current-line(parse-index:1) TO char-as-int
                   MULTIPLY 10 BY part-number(part-category-index)
                   ADD char-as-int TO part-number(part-category-index)
                   ADD 1 TO parse-index
               END-PERFORM
               ADD 1 TO parse-index
               ADD 1 TO part-category-index
           END-PERFORM
           .
       
       is-accepted.
           MOVE "in" TO current-workflow-name
           PERFORM UNTIL (current-workflow-name = "A") OR (current-workflow-name = "R")
               PERFORM search-workflow
               MOVE 0 TO temp-bool
               PERFORM VARYING rule-index FROM 1 BY 1 UNTIL (category(workflow-index,rule-index) = " ") OR (temp-bool = 1)
                   EVALUATE category(workflow-index,rule-index)
                    WHEN "x" MOVE 1 TO part-category-index
                    WHEN "m" MOVE 2 TO part-category-index
                    WHEN "a" MOVE 3 TO part-category-index
                    WHEN "s" MOVE 4 TO part-category-index
                   END-EVALUATE
                   IF is-greater(workflow-index,rule-index) = 0
                       IF part-number(part-category-index) < threshold(workflow-index,rule-index)
                           MOVE dest(workflow-index,rule-index) TO current-workflow-name
                           MOVE 1 TO temp-bool
                       END-IF
                   ELSE
                       IF part-number(part-category-index) > threshold(workflow-index,rule-index)
                           MOVE dest(workflow-index,rule-index) TO current-workflow-name
                           MOVE 1 TO temp-bool
                       END-IF
                   END-IF
               END-PERFORM 
               IF temp-bool = 0
                   MOVE wf-default(workflow-index) TO current-workflow-name
               END-IF
           END-PERFORM
           .
       
       search-workflow.
           PERFORM VARYING workflow-index FROM 1 BY 1 UNTIL wf-name(workflow-index) = current-workflow-name
               CONTINUE
           END-PERFORM
           .
END PROGRAM AOC-DAY-19.
