IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-3.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day3/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 next-line PIC X(200).

       WORKING-STORAGE SECTION.
       77 end-of-file PIC Z.
       77 current-line PIC X(200).
       77 previous-line PIC X(200).

       77 parse-position PIC 999.
       77 gear-position PIC 999.
       77 first-gear-number PIC 9(6).
       77 second-gear-number PIC 999.
       77 too-many-gears PIC 9.
       77 number-before PIC 999.
       77 number-after PIC 999.
       77 buffer PIC X(7).
       77 buffer-index PIC 9.
       77 buffer-first-number PIC 999.
       77 buffer-finished-first-number PIC 9.
       77 buffer-second-number PIC 999.
       77 total PIC 9(11) VALUE ZEROES.

       77 char-as-int PIC 9.
       77 stop-bool PIC 9.
       *> intermediate values
       77 num1 PIC 999.
       77 num2 PIC 999.

PROCEDURE DIVISION.
       MOVE ALL "." TO current-line
       MOVE ALL "." TO previous-line
       MOVE ALL "." TO next-line
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           PERFORM process-current-line
           
           MOVE current-line TO previous-line
           MOVE next-line TO current-line
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM

       PERFORM process-current-line

       DISPLAY "TOTAL = " total
       
       CLOSE file-fd
       STOP RUN.

       process-current-line.
           MOVE 1 TO parse-position
           PERFORM find-next-gear
           PERFORM UNTIL parse-position > 200
               MOVE 0 TO first-gear-number
               MOVE 0 TO second-gear-number
               MOVE 0 TO too-many-gears
               
               MOVE gear-position TO parse-position
               PERFORM parse-number-before
               PERFORM parse-number-after
               IF NOT number-before = 0
                   MOVE number-before TO first-gear-number
               END-IF
               IF NOT number-after = 0
                   IF first-gear-number = 0
                       MOVE number-after TO first-gear-number
                   ELSE
                       MOVE number-after TO second-gear-number
                   END-IF
               END-IF
               
               *> Here:
               *> - num1 is the length of the buffer (max 7)
               *> - num2 is the offset into buffer (usually just 1)
               MOVE 7 TO num1
               MOVE 1 TO num2
               IF parse-position < 4
                   MOVE 3 to num1
                   MOVE 5 to num2
                   ADD parse-position TO num1
                   SUBTRACT parse-position FROM num2
                   MOVE 1 TO parse-position
               ELSE
                   SUBTRACT 3 FROM parse-position
               END-IF
               
               MOVE ALL "." TO buffer
               MOVE previous-line(parse-position:num1) TO buffer(num2:num1)
               PERFORM 2 TIMES
                   PERFORM parse-numbers-in-buffer
                   PERFORM 2 TIMES
                       IF NOT buffer-first-number = 0
                       IF first-gear-number = 0
                           MOVE buffer-first-number TO first-gear-number
                       ELSE
                           IF NOT second-gear-number = 0
                               MOVE 1 TO too-many-gears
                           END-IF
                           MOVE buffer-first-number TO second-gear-number
                       END-IF
                       MOVE buffer-second-number TO buffer-first-number
                   END-PERFORM
                   MOVE ALL "." TO buffer
                   MOVE next-line(parse-position:num1) TO buffer(num2:num1)
               END-PERFORM

               IF too-many-gears = 0 AND NOT (second-gear-number = 0)
                   DISPLAY "first gear number = " first-gear-number ", second gear number = " second-gear-number
                   MULTIPLY second-gear-number BY first-gear-number
                   ADD first-gear-number TO total
               END-IF
               
               MOVE gear-position TO parse-position
               ADD 1 TO parse-position
               PERFORM find-next-gear
           END-PERFORM.
       
       *> Moves `gear-position` and `parse-position` to the position of the next '*' character
       *>
       *> If no such gear exists, set `parse-position` to a value > 200
       find-next-gear.
           MOVE 0 TO stop-bool
           PERFORM UNTIL stop-bool = 1
               IF (current-line(parse-position:1) = "*") OR (parse-position > 200)
                   MOVE parse-position TO gear-position
                   MOVE 1 TO stop-bool
               ELSE
                   ADD 1 TO parse-position
               END-IF
           END-PERFORM.
       
       *> Assuming there is a gear at `gear-position`, parse a number right before the 
       *> gear, and put the result in `number-before`.
       *>
       *> If there are no such numbers, put 0 in 
       parse-number-before.
           MOVE 1 TO num1
           MOVE 0 TO stop-bool
           MOVE 0 TO number-before
           SUBTRACT 1 FROM parse-position
           PERFORM UNTIL stop-bool = 1
               IF parse-position > 0
                   MOVE current-line(parse-position:1) TO char-as-int
                   IF (current-line(parse-position:1) = "0") OR NOT (char-as-int = 0)
                       MOVE char-as-int TO num2
                       MULTIPLY num1 BY num2
                       ADD num2 TO number-before
                       MULTIPLY 10 BY num1
                   ELSE
                       MOVE 1 TO stop-bool
                   END-IF
                   SUBTRACT 1 FROM parse-position
               ELSE
                   MOVE 1 TO stop-bool
               END-IF
           END-PERFORM
           MOVE gear-position TO parse-position.
       
       *> Assuming there is a gear at `gear-position`, parse a number right after the 
       *> gear, and put the result in `number-after`.
       *>
       *> If there are no such numbers, put 0 in 
       parse-number-after.
           MOVE 0 TO stop-bool
           MOVE 0 TO number-after
           ADD 1 TO parse-position
           PERFORM UNTIL stop-bool = 1
               MOVE current-line(parse-position:1) TO char-as-int
               IF (current-line(parse-position:1) = "0") OR NOT (char-as-int = 0)
                   MULTIPLY 10 BY number-after
                   ADD char-as-int TO number-after
               ELSE
                   MOVE 1 TO stop-bool
               END-IF
               ADD 1 TO parse-position
           END-PERFORM
           MOVE gear-position TO parse-position.
       
       *> Parse the one or two numbers present in `buffer`, that are adjacent with the
       *> gear position (which is 4)
       *> 
       *> Puts the eventual results in `buffer-first-number` and `buffer-second-number`
       *> These are 0 if numbers were not found.
       parse-numbers-in-buffer.
           MOVE 1 TO buffer-index
           MOVE 0 TO buffer-first-number
           MOVE 0 TO buffer-second-number
           MOVE 0 TO buffer-finished-first-number
           PERFORM 7 TIMES
               MOVE buffer(buffer-index:1) TO char-as-int
               IF (buffer(buffer-index:1) = "0") OR NOT (char-as-int = 0)
                   IF buffer-finished-first-number = 0
                       MULTIPLY 10 BY buffer-first-number
                       ADD char-as-int TO buffer-first-number
                   ELSE
                       MULTIPLY 10 BY buffer-second-number
                       ADD char-as-int TO buffer-second-number
                   END-IF
               ELSE
                   IF buffer-index < 4
                       *> 12.....
                       *>    *
                       *> 12 is not a gear number
                       MOVE 0 TO buffer-first-number
                   END-IF
                   IF buffer-index > 4
                       *> .....34
                       *>    *
                       *> 34 is not a gear number
                       MOVE ALL "." TO buffer
                   END-IF
                   IF NOT buffer-first-number = 0
                       MOVE 1 TO buffer-finished-first-number
                   END-IF
               END-IF
               ADD 1 TO buffer-index
           END-PERFORM.
END PROGRAM AOC-DAY-3.
