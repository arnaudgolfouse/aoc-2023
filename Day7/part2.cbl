IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-7.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day7/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line.
           02 input-hand    PIC X(5).
           02 IGNORED-space PIC X.
           02 input-bid     PIC X(4).

       WORKING-STORAGE SECTION.
       REPLACE ==NumberOfHandsPlusTemp== BY ==1001==.
       *> The last one is a temporary for swap purposes
       01 hands OCCURS NumberOfHandsPlusTemp TIMES INDEXED BY hand-index.
           02 hand-data PIC X OCCURS 5 TIMES.
           02 hand-kind PIC 9 USAGE COMPUTATIONAL.
           02 bid       PIC 9(4) USAGE COMPUTATIONAL.
       77 hand-index-2 PIC 9(4) USAGE COMPUTATIONAL.
       77 hand-index-3 PIC 9(4) USAGE COMPUTATIONAL.

       77 total PIC 9(16) USAGE COMPUTATIONAL.
       
       *> Parsing
       77 nb-of-equal-cards PIC 9 OCCURS 5 TIMES INDEXED BY card-index.
       77 highest-equal-cards PIC 9. 
       77 highest-equal-index PIC 9. 
       77 card-index-2  PIC 9.
       77 end-of-file PIC 9.

       *> Comparison
       77 comp-card-1 PIC X.
       77 comp-card-2 PIC X.
       77 compare-result PIC S9 USAGE COMPUTATIONAL.
       
       *> Temporary variables
       77 temp-bool   PIC 9    USAGE COMPUTATIONAL.
       77 temp-char   PIC X.
       77 num1        PIC 9(8) USAGE COMPUTATIONAL.
       77 num2        PIC 9(8) USAGE COMPUTATIONAL.
PROCEDURE DIVISION.
       MOVE 1 TO hand-index
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           PERFORM process-line
           ADD 1 TO hand-index
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       
       CLOSE file-fd
       
       PERFORM sort-hands

       MOVE 0 TO total
       PERFORM VARYING hand-index FROM 1 BY 1 UNTIL hand-index >= NumberOfHandsPlusTemp
           COMPUTE total = total + hand-index * bid(hand-index)
       END-PERFORM

       DISPLAY total

       STOP RUN.

       process-line.
           PERFORM VARYING card-index FROM 1 BY 1 UNTIL card-index > 5
               MOVE input-hand(card-index:1) TO hand-data(hand-index,card-index)
           END-PERFORM

           UNSTRING input-bid DELIMITED BY ALL SPACE
               INTO bid(hand-index)
           
           PERFORM determine-equal-cards
           
           IF highest-equal-cards = 0
               *> 5 'J'
               MOVE 5 TO highest-equal-cards
           ELSE
               PERFORM VARYING card-index FROM 1 BY 1 UNTIL card-index > 5
                   IF input-hand(card-index:1) = "J"
                       MOVE input-hand(highest-equal-index:1) TO input-hand(card-index:1)
                   END-IF
               END-PERFORM
               
               PERFORM determine-equal-cards
           END-IF
           
           IF highest-equal-cards = 5
               *> Five of a kind
               MOVE 7 TO hand-kind(hand-index)
           END-IF
           IF highest-equal-cards = 4
               *> Four of a kind
               MOVE 6 TO hand-kind(hand-index)
           END-IF
           IF highest-equal-cards = 3
               MOVE 0 TO temp-bool
               PERFORM VARYING card-index FROM 1 BY 1 UNTIL card-index > 5
                   IF nb-of-equal-cards(card-index) = 2
                       MOVE 1 TO temp-bool
                   END-IF
               END-PERFORM
               IF temp-bool = 1
                   *> Full house
                   MOVE 5 TO hand-kind(hand-index)
               ELSE
                   *> Three of a kind
                   MOVE 4 TO hand-kind(hand-index)
               END-IF
           END-IF
           IF highest-equal-cards = 2
               MOVE 0 TO num1
               PERFORM VARYING card-index FROM 1 BY 1 UNTIL card-index > 5
                   IF nb-of-equal-cards(card-index) = 2
                       ADD 1 TO num1
                   END-IF
               END-PERFORM
               IF num1 = 4
                   *> Two pairs
                   MOVE 3 TO hand-kind(hand-index)
               ELSE
                   *> One pair
                   MOVE 2 TO hand-kind(hand-index)
               END-IF
           END-IF
           IF highest-equal-cards = 1
               *> High card
               MOVE 1 TO hand-kind(hand-index)
           END-IF
           .
       
       *> For each card in `input-hand`, fill `nb-of-equal-cards` with the number of cards equal to it (including itself).
       *> 
       *> Also, at the end of this procedure,
       *> - `highest-equal-cards` is the highest value of `nb-of-equal-cards`
       *> - `highest-equal-index` is the index of this value in `nb-of-equal-cards`.
       *> 
       *> # Example
       *> If `input-hand` is "AA944", `nb-of-equal-cards` is [2, 2, 1, 2, 2]
       determine-equal-cards.
           PERFORM VARYING card-index FROM 1 BY 1 UNTIL card-index > 5
               MOVE 0 TO nb-of-equal-cards(card-index)
           END-PERFORM
           MOVE 0 TO highest-equal-cards
           PERFORM VARYING card-index FROM 1 BY 1 UNTIL card-index > 5
               PERFORM VARYING card-index-2 FROM 1 BY 1 UNTIL card-index-2 > 5
                   IF input-hand(card-index:1) = input-hand(card-index-2:1)
                       ADD 1 TO nb-of-equal-cards(card-index)
                   END-IF
               END-PERFORM
               IF NOT (input-hand(card-index:1) = "J") AND (nb-of-equal-cards(card-index) > highest-equal-cards)
                   MOVE card-index TO highest-equal-index
                   MOVE nb-of-equal-cards(card-index) TO highest-equal-cards
               END-IF
           END-PERFORM
           .
       
       sort-hands.
           PERFORM VARYING hand-index FROM 2 BY 1 UNTIL hand-index >= NumberOfHandsPlusTemp
               PERFORM VARYING hand-index-2 FROM hand-index BY -1 UNTIL hand-index-2 <= 1
                   MOVE hand-index-2 TO hand-index-3
                   SUBTRACT 1 FROM hand-index-3
                   PERFORM compare-hands
                   IF compare-result = -1
                       *> Swap
                        MOVE hands(hand-index-2) TO hands(NumberOfHandsPlusTemp)
                        MOVE hands(hand-index-3) TO hands(hand-index-2)
                        MOVE hands(NumberOfHandsPlusTemp) TO hands(hand-index-3)
                   ELSE
                       MOVE 1 TO hand-index-2
                   END-IF
               END-PERFORM
           END-PERFORM
           .
       
       *> Compare the hands at `hand-index-2` and `hand-index-3`, and put the result in `compare-result`.
       compare-hands.
           IF hand-kind(hand-index-2) > hand-kind(hand-index-3)
               MOVE 1 TO compare-result
           END-IF
           IF hand-kind(hand-index-2) < hand-kind(hand-index-3)
               MOVE -1 TO compare-result
           END-IF
           IF hand-kind(hand-index-2) = hand-kind(hand-index-3)
               PERFORM VARYING card-index FROM 1 BY 1 UNTIL card-index > 5
                   MOVE hand-data(hand-index-2,card-index) TO comp-card-1
                   MOVE hand-data(hand-index-3,card-index) TO comp-card-2
                   PERFORM compare-cards
                   IF NOT (compare-result = 0)
                       MOVE 6 TO card-index
                   END-IF
               END-PERFORM
           END-IF
           .
       
       *> - if `comp-card-1` > `comp-card-2`, put 1 into `compare-result` ;
       *> - if `comp-card-1` < `comp-card-2`, put -1 into `compare-result` ;
       *> - else, put 0 into `compare-result` ;
       compare-cards.
           MOVE comp-card-2 TO temp-char
           PERFORM get-card-score
           MOVE num1 TO num2
           MOVE comp-card-1 TO temp-char
           PERFORM get-card-score
           MOVE 0 TO compare-result
           IF num1 > num2
               MOVE 1 TO compare-result
           END-IF
           IF num1 < num2
               MOVE -1 TO compare-result
           END-IF
           .
       
       *> Put the score of `temp-char` in `num1`
       get-card-score.
           MOVE temp-char TO num1
           IF num1 = 0
               EVALUATE temp-char
                   WHEN "T" MOVE 10 TO num1
                   WHEN "J" MOVE 0 TO num1
                   WHEN "Q" MOVE 12 TO num1
                   WHEN "K" MOVE 13 TO num1
                   WHEN "A" MOVE 14 TO num1
                   WHEN OTHER DISPLAY "ERROR: found other card '" temp-char "'"
               END-EVALUATE
           END-IF
           .
END PROGRAM AOC-DAY-7.
