
IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-5.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day5/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line   PIC X(300).

       WORKING-STORAGE SECTION.
       77 seeds           PIC 9(12) OCCURS 20 TIMES INDEXED BY seeds-index.
       77 seeds-processed PIC 9     OCCURS 20 TIMES.
       77 convert-range   PIC 9(12) OCCURS 03 TIMES INDEXED BY range-index.
       77 source-range    PIC 9(12) USAGE COMPUTATIONAL.
       77 dest-range      PIC 9(12) USAGE COMPUTATIONAL.
       77 range-length    PIC 9(12) USAGE COMPUTATIONAL.
       77 range-end       PIC 9(12) USAGE COMPUTATIONAL.
       77 range-diff      PIC 9(12) USAGE COMPUTATIONAL.
       77 range-decrease  PIC 9     USAGE COMPUTATIONAL.
       77 lowest-location PIC 9(10) USAGE COMPUTATIONAL.

       *> parsing variables
       77 parse-position  PIC 999   USAGE COMPUTATIONAL.
       77 char-as-int     PIC S9    USAGE COMPUTATIONAL.
       77 parsed-number   PIC 9(12) USAGE COMPUTATIONAL.
       77 end-of-file     PIC 9     USAGE COMPUTATIONAL.
       77 end-of-section  PIC 9     USAGE COMPUTATIONAL.

PROCEDURE DIVISION.
       MOVE 0 TO end-of-file
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM initialize-seeds.
       
       READ file-fd *> newline

       PERFORM UNTIL end-of-file = 1
           READ file-fd *> skip the section title

           MOVE 0 TO end-of-section
           MOVE 1 TO seeds-index
           PERFORM 20 TIMES
               MOVE 0 TO seeds-processed(seeds-index)
               ADD 1 TO seeds-index
           END-PERFORM
           READ file-fd
           PERFORM read-range
           PERFORM UNTIL end-of-section = 1
               *> main logic
               MOVE 1 TO seeds-index
               PERFORM 20 TIMES
                   IF seeds-processed(seeds-index) = 0
                       IF (seeds(seeds-index) >= source-range) AND (seeds(seeds-index) < range-end)
                           IF range-decrease = 0
                               ADD range-diff TO seeds(seeds-index)
                           ELSE
                               SUBTRACT range-diff FROM seeds(seeds-index)
                           END-IF
                           MOVE 1 TO seeds-processed(seeds-index)
                       END-IF
                   END-IF
                   ADD 1 TO seeds-index
               END-PERFORM

               *> Read the next line
               READ file-fd
                   AT END MOVE 1 TO end-of-file
               END-READ
               IF end-of-file = 0
                   PERFORM read-range
               ELSE
                   MOVE 1 TO end-of-section
               END-IF
           END-PERFORM
       END-PERFORM

       MOVE 1 TO seeds-index
       MOVE seeds(1) TO lowest-location
       PERFORM 20 TIMES
        *>    DISPLAY "seed " seeds-index " = " seeds(seeds-index)
           IF seeds(seeds-index) < lowest-location
               MOVE seeds(seeds-index) TO lowest-location
           END-IF
           ADD 1 TO seeds-index
       END-PERFORM
       
       DISPLAY "lowest-location = " lowest-location
       
       CLOSE file-fd
       STOP RUN.

       initialize-seeds.
           *> Start after "seeds: "
           MOVE 8 TO parse-position
           MOVE 1 TO seeds-index
           PERFORM 20 TIMES
               PERFORM parse-number
               ADD 1 TO parse-position *> skip the space
               MOVE parsed-number TO seeds(seeds-index)
               ADD 1 TO seeds-index
           END-PERFORM
           .
       
       read-range.
           MOVE 1 TO parse-position
           MOVE 1 TO range-index
           PERFORM 3 TIMES
               PERFORM parse-number
               MOVE parsed-number TO convert-range(range-index)
               ADD 1 TO range-index
               IF NOT (parse-position = 1)
                   ADD 1 TO parse-position
               END-IF
           END-PERFORM
           IF (parse-position = 1)
               MOVE 1 TO end-of-section
           ELSE
               MOVE convert-range(1) TO dest-range
               MOVE convert-range(2) TO source-range
               MOVE convert-range(3) TO range-length
               MOVE source-range     TO range-end
               ADD  range-length     TO range-end
               IF source-range < dest-range
                   MOVE dest-range       TO range-diff
                   SUBTRACT source-range FROM range-diff
                   MOVE 0 TO range-decrease
               ELSE
                   MOVE source-range   TO range-diff
                   SUBTRACT dest-range FROM range-diff
                   MOVE 1 TO range-decrease
               END-IF 
           END-IF
           .
       
       *> Parse a number in the current line, and put the result in `parsed-number`.
       *> After this function, `parse-position` will be right after the parsed number.
       parse-number.
           MOVE 0 TO parsed-number
           MOVE current-line(parse-position:1) TO char-as-int
           PERFORM UNTIL (NOT (current-line(parse-position:1) = "0")) AND (char-as-int = 0)
               MULTIPLY 10 BY parsed-number
               ADD char-as-int TO parsed-number
               ADD 1 TO parse-position
               MOVE current-line(parse-position:1) TO char-as-int
           END-PERFORM
           .
END PROGRAM AOC-DAY-5.
