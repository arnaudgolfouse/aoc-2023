IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-15.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day15/input.txt"
            ORGANIZATION IS SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-char PIC X.

       WORKING-STORAGE SECTION.
       77 end-of-file PIC 9 VALUE 0.
       77 hash-value PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
       77 operation-kind PIC X VALUE " ".
       77 operation-number PIC 9.

       77 current-label PIC X(9).
       77 current-label-len PIC 9 VALUE 0.

       01 boxes OCCURS 256 TIMES INDEXED BY box-index.
           02 lenses-number USAGE INDEX VALUE 0.
           02 lens-slots OCCURS 100 TIMES INDEXED BY slot-index.
               03 lens-label     PIC X(9).
               03 lens-label-len PIC 9.
               03 focal-length   PIC 9.
       
       77 found-lens-at USAGE INDEX.
       
       77 focusing-power PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
       77 result PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           IF current-char = "-" OR "="
               MOVE current-char TO operation-kind
               IF current-char = "="
                   READ file-fd
                   MOVE current-char TO operation-number
               END-IF
           END-IF
           IF operation-kind = " "
               ADD 1 TO current-label-len
               MOVE current-char TO current-label(current-label-len:1)
               COMPUTE hash-value = FUNCTION MOD (17 * (hash-value + FUNCTION ORD (current-char) - 1), 256)
           END-IF
           IF current-char = ","
               PERFORM apply-operation
               MOVE " " TO operation-kind
               MOVE 0 TO hash-value
               MOVE 0 TO current-label-len
           END-IF
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       CLOSE file-fd

       PERFORM apply-operation

       PERFORM compute-focusing-power
       
       DISPLAY "result = " result

       STOP RUN.

       apply-operation.
           COMPUTE box-index = hash-value + 1
           MOVE 0 TO found-lens-at
           PERFORM VARYING slot-index FROM 1 BY 1 UNTIL slot-index > lenses-number(box-index)
               IF lens-label(box-index,slot-index) (1:lens-label-len(box-index,slot-index)) = current-label(1:current-label-len)
                   MOVE slot-index TO found-lens-at
               END-IF
           END-PERFORM
           IF operation-kind = "="
               IF found-lens-at = 0
                   ADD 1 TO lenses-number(box-index)
                   MOVE lenses-number(box-index) TO slot-index
                   MOVE current-label     TO lens-label(box-index,slot-index)
                   MOVE current-label-len TO lens-label-len(box-index,slot-index)
                   MOVE operation-number  TO focal-length(box-index,slot-index)
               ELSE
                   MOVE operation-number  TO focal-length(box-index,found-lens-at)
               END-IF
           ELSE
               IF NOT found-lens-at = 0
                   PERFORM VARYING slot-index FROM found-lens-at BY 1 UNTIL slot-index >= lenses-number(box-index)
                       MOVE lens-slots(box-index,slot-index + 1) TO lens-slots(box-index,slot-index)
                   END-PERFORM
                   SUBTRACT 1 FROM lenses-number(box-index)
               END-IF
           END-IF
           .
       
       compute-focusing-power.
           PERFORM VARYING box-index FROM 1 BY 1 UNTIL box-index > 256
               PERFORM VARYING slot-index FROM 1 BY 1 UNTIL slot-index > lenses-number(box-index)
                   COMPUTE focusing-power = box-index * slot-index * focal-length(box-index,slot-index)
                   ADD focusing-power TO result
               END-PERFORM
           END-PERFORM
           .
END PROGRAM AOC-DAY-15.
