IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-4.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day4/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 next-line.
           02 IGNORED-card PIC X(5).
           02 card-number PIC 999.
           02 IGNORED-colon PIC X.
           02 winning-numbers OCCURS 10 TIMES INDEXED BY winning-numbers-index.
               03 IGNORED-space PIC X.
               03 winning-number PIC 99.
           02 IGNORED-separator PIC X(2).
           02 my-numbers OCCURS 25 TIMES INDEXED BY my-numbers-index.
               03 IGNORED-space PIC X.
               03 my-number PIC 99.

       WORKING-STORAGE SECTION.
       77 is-winning-number PIC Z.
       77 line-score PIC 9(4).
       77 total PIC 9(7).
       77 end-of-file PIC Z.
       

PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           PERFORM process-line
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM

       DISPLAY total
       
       CLOSE file-fd
       STOP RUN.

       process-line.
           MOVE 0 TO line-score
           MOVE 1 TO winning-numbers-index
           PERFORM 10 TIMES
               MOVE 0 TO is-winning-number
               MOVE 1 TO my-numbers-index
               PERFORM 25 TIMES
                   IF my-numbers(my-numbers-index) = winning-numbers(winning-numbers-index)
                       IF line-score = 0
                           MOVE 1 TO line-score
                       ELSE
                           MULTIPLY 2 BY line-score
                       END-IF
                   END-IF
                   ADD 1 TO my-numbers-index
               END-PERFORM
               ADD 1 TO winning-numbers-index
           END-PERFORM
           ADD line-score TO total.
END PROGRAM AOC-DAY-4.
