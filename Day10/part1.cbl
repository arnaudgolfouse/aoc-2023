IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-10.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day10/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(140).

       WORKING-STORAGE SECTION.
       REPLACE ==LineLength== BY ==140==.
       77 pipes PIC X(LineLength) OCCURS LineLength TIMES.
       77 pipe-line  USAGE INDEX.
       77 pipe-col   USAGE INDEX.
       77 start-line USAGE INDEX.
       77 start-col  USAGE INDEX.
       01 comes-from PIC X.
       77 steps-counts PIC 9(16) USAGE COMPUTATIONAL.
       77 result       PIC 9(16) USAGE COMPUTATIONAL.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       PERFORM VARYING pipe-line FROM 1 BY 1 UNTIL pipe-line > LineLength
           READ file-fd
           MOVE current-line TO pipes(pipe-line)
       END-PERFORM
       CLOSE file-fd

       PERFORM VARYING pipe-line FROM 1 BY 1 UNTIL pipe-line > LineLength
           PERFORM VARYING pipe-col FROM 1 BY 1 UNTIL pipe-col > LineLength
               IF pipes(pipe-line) (pipe-col:1) = "S"
                   MOVE pipe-line TO start-line
                   MOVE pipe-col TO start-col
               END-IF
           END-PERFORM
       END-PERFORM

       PERFORM find-start-position

       PERFORM UNTIL pipes(pipe-line) (pipe-col:1) = "S"
           PERFORM find-next-position
           ADD 1 TO steps-counts
       END-PERFORM

       COMPUTE result = steps-counts / 2

       DISPLAY result
       
       STOP RUN.

       find-start-position.
           MOVE 1 TO steps-counts
           MOVE start-col TO pipe-col
           COMPUTE pipe-line = start-line - 1
           IF (pipes(pipe-line) (pipe-col:1) = "|") OR (pipes(pipe-line) (pipe-col:1) = "J") OR (pipes(pipe-line) (pipe-col:1) = "L")
               MOVE "t" TO comes-from
           END-IF
           COMPUTE pipe-line = start-line + 1
           IF (pipes(pipe-line) (pipe-col:1) = "|") OR (pipes(pipe-line) (pipe-col:1) = "F") OR (pipes(pipe-line) (pipe-col:1) = "7")
               MOVE "b" TO comes-from
           END-IF
           MOVE start-line TO pipe-line
           COMPUTE pipe-col = start-col - 1
           IF (pipes(pipe-line) (pipe-col:1) = "-") OR (pipes(pipe-line) (pipe-col:1) = "F") OR (pipes(pipe-line) (pipe-col:1) = "L")
               MOVE "r" TO comes-from
           END-IF
           COMPUTE pipe-col = start-col - 1
           IF (pipes(pipe-line) (pipe-col:1) = "-") OR (pipes(pipe-line) (pipe-col:1) = "J") OR (pipes(pipe-line) (pipe-col:1) = "7")
               MOVE "l" TO comes-from
           END-IF
           
           MOVE start-col TO pipe-col
           EVALUATE comes-from
               WHEN "l" COMPUTE pipe-col  = start-col  + 1
               WHEN "r" COMPUTE pipe-col  = start-col  - 1
               WHEN "t" COMPUTE pipe-line = start-line + 1
               WHEN "b" COMPUTE pipe-line = start-line - 1
           END-EVALUATE
           .
       
       find-next-position.
           EVALUATE pipes(pipe-line) (pipe-col:1)
               WHEN "-" IF comes-from = "l"
                       ADD 1 TO pipe-col
                   ELSE
                       SUBTRACT 1 FROM pipe-col
                   END-IF
               WHEN "|" IF comes-from = "t"
                       ADD 1 TO pipe-line
                   ELSE
                       SUBTRACT 1 FROM pipe-line
                   END-IF
               WHEN "L" IF comes-from = "t"
                       ADD 1 TO pipe-col
                       MOVE "l" TO comes-from
                   ELSE
                       SUBTRACT 1 FROM pipe-line
                       MOVE "b" TO comes-from
                   END-IF
               WHEN "J" IF comes-from = "t"
                       SUBTRACT 1 FROM pipe-col
                       MOVE "r" TO comes-from
                   ELSE
                       SUBTRACT 1 FROM pipe-line
                       MOVE "b" TO comes-from
                   END-IF
               WHEN "7" IF comes-from = "b"
                       SUBTRACT 1 FROM pipe-col
                       MOVE "r" TO comes-from
                   ELSE
                       ADD 1 TO pipe-line
                       MOVE "t" TO comes-from
                   END-IF
               WHEN "F" IF comes-from = "b"
                       ADD 1 TO pipe-col
                       MOVE "l" TO comes-from
                   ELSE
                       ADD 1 TO pipe-line
                       MOVE "t" TO comes-from
                   END-IF
           .
END PROGRAM AOC-DAY-10.
