IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-21.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day21/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       REPLACE ==StepsNb== BY ==64== ;
               ==MapSize== BY ==131== ;
               ==StackSize== BY ==10000==.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(MapSize).

       WORKING-STORAGE SECTION.
       77 map PIC X(MapSize) OCCURS MapSize TIMES INDEXED BY map-line.
       77 map-col       INDEX.

       01 FILLER OCCURS MapSize TIMES.
           02 FILLER OCCURS MapSize TIMES.
               03 is-visited PIC 9 COMP VaLUE 0.

       01 FILLER OCCURS MapSize TIMES.
           02 FILLER OCCURS MapSize TIMES.
               03 distance           PIC 9(16) COMP VALUE 9999999999999999.
               03 previous-line      PIC 999 COMP.
               03 previous-col       PIC 999 COMP.
               03 index-in-priority  INDEX VALUE -1.
       
       77 start-line INDEX.
       77 start-col  INDEX.

       77 next-map-line  INDEX.
       77 next-map-col   INDEX.
       77 next-distance  PIC 9(16) COMP VALUE 9999999999999999.

       *> smallest one at the top
       01 priority-stack OCCURS StackSize TIMES INDEXED BY prio-index.
           02 prio-line      PIC 999   COMP.
           02 prio-col       PIC 999   COMP.
        *>    02 prio-distance  PIC 9(16) COMP.
       77 prio-length INDEX VALUE 0.
       
       77 temp-n-1  PIC 9(16) COMP.
       77 temp-n-2  PIC 9(16) COMP.
       77 temp-n-3  PIC 9(16) COMP.
       77 result    PIC 9(16) COMP.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       PERFORM VARYING map-line FROM 1 BY 1 UNTIL map-line > MapSize
           READ file-fd
           MOVE current-line TO map(map-line)
           PERFORM VARYING map-col FROM 1 BY 1 UNTIL map-col > MapSize
               IF current-line(map-col:1) = "S"
                   MOVE map-line TO start-line
                   MOVE map-col TO start-col
               END-IF
           END-PERFORM
       END-PERFORM
       CLOSE file-fd

       PERFORM fill-unreachable

       MOVE 0 TO result
       PERFORM VARYING map-line FROM 1 BY 1 UNTIL map-line > MapSize
           PERFORM VARYING map-col FROM 1 BY 1 UNTIL map-col > MapSize
               COMPUTE temp-n-1 = FUNCTION ABS(map-line - start-line) + FUNCTION ABS(map-col - start-col)
               COMPUTE temp-n-2 = FUNCTION MOD(temp-n-1,2)
               IF NOT (map(map-line)(map-col:1) = "#") AND (temp-n-2 = 0) AND (temp-n-1 <= StepsNb)
                   ADD 1 TO result
               END-IF
           END-PERFORM
       END-PERFORM

       DISPLAY "result = " result
       
       STOP RUN.

       *> Fill non-reachable "." with "#"
       fill-unreachable.
           MOVE 1 TO prio-length
           MOVE start-line TO prio-line(1)
           MOVE start-col TO prio-col(1)
           PERFORM UNTIL prio-length = 0
               MOVE prio-line(prio-length) TO map-line
               MOVE prio-col(prio-length) TO map-col
               SUBTRACT 1 FROM prio-length
               PERFORM VARYING temp-n-3 FROM 1 BY 1 UNTIL temp-n-3 > 4
                   MOVE map-line TO next-map-line
                   MOVE map-col TO next-map-col
                   EVALUATE temp-n-3
                    WHEN 1 SUBTRACT 1 FROM next-map-line
                    WHEN 2 ADD 1 TO next-map-line
                    WHEN 3 SUBTRACT 1 FROM next-map-col
                    WHEN 4 ADD 1 TO next-map-col
                   END-EVALUATE

                   IF (next-map-line < 1) OR (next-map-line > MapSize) OR (next-map-col < 1) OR (next-map-col > MapSize)
                       CONTINUE
                   ELSE
                       IF (is-visited(next-map-line,next-map-col) = 0) AND (map(next-map-line)(next-map-col:1) = ".")
                           MOVE 1 TO is-visited(next-map-line,next-map-col)
                           ADD 1 TO prio-length
                           MOVE next-map-line TO prio-line(prio-length)
                           MOVE next-map-col TO prio-col(prio-length)
                       END-IF
                   END-IF
               END-PERFORM
           END-PERFORM

           PERFORM VARYING map-line FROM 1 BY 1 UNTIL map-line > MapSize
               PERFORM VARYING map-col FROM 1 BY 1 UNTIL map-col > MapSize
                   IF (map(map-line)(map-col:1) = ".") AND (is-visited(map-line,map-col) = 0)
                       MOVE "#" TO map(map-line)(map-col:1)
                   END-IF
               END-PERFORM
           END-PERFORM
           .
END PROGRAM AOC-DAY-21.
