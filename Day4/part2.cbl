IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-4.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day4/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 next-line.
           02 IGNORED-card PIC X(5).
           02 card-number PIC 999.
           02 IGNORED-colon PIC X.
           02 winning-numbers OCCURS 10 TIMES INDEXED BY winning-numbers-index.
               03 IGNORED-space PIC X.
               03 winning-number PIC 99.
           02 IGNORED-separator PIC X(2).
           02 my-numbers OCCURS 25 TIMES INDEXED BY my-numbers-index.
               03 IGNORED-space PIC X.
               03 my-number PIC 99.

       WORKING-STORAGE SECTION.
       01 winning-numbers-per-card PIC 9(2) OCCURS 205 TIMES INDEXED BY card-index.
       01 number-of-cards PIC 9(10) OCCURS 220 TIMES INDEXED BY next-card-index.
       77 total PIC 9(10).
       77 end-of-file PIC Z.
       

PROCEDURE DIVISION.
       MOVE 1 TO card-index
       PERFORM 205 TIMES
           MOVE 0 TO winning-numbers-per-card(card-index)
           MOVE 1 TO number-of-cards(card-index)
           ADD 1 TO card-index
       END-PERFORM

       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           PERFORM process-line
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM

       *> Now we now how many winning numbers there are on each line
       
       MOVE 1 TO card-index
       MOVE 0 TO total
       PERFORM 205 TIMES
           PERFORM duplicate-card
           ADD number-of-cards(card-index) TO total
           ADD 1 TO card-index
       END-PERFORM

       DISPLAY total
       
       CLOSE file-fd
       STOP RUN.

       process-line.
           MOVE 1 TO winning-numbers-index
           PERFORM 10 TIMES
               MOVE 1 TO my-numbers-index
               PERFORM 25 TIMES
                   IF my-numbers(my-numbers-index) = winning-numbers(winning-numbers-index)
                       MOVE card-number TO card-index
                       ADD 1 TO winning-numbers-per-card(card-index)
                   END-IF
                   ADD 1 TO my-numbers-index
               END-PERFORM
               ADD 1 TO winning-numbers-index
           END-PERFORM.
       
       duplicate-card.
           MOVE card-index TO next-card-index
           ADD 1 TO next-card-index
           PERFORM winning-numbers-per-card(card-index) TIMES
               ADD number-of-cards(card-index) TO number-of-cards(next-card-index)
               ADD 1 TO next-card-index
           END-PERFORM.
END PROGRAM AOC-DAY-4.
