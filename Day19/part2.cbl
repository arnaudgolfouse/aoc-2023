IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-19.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day19/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(50).

       WORKING-STORAGE SECTION.
       77 parse-index INDEX.
       77 char-as-int PIC 9.
       77 end-of-file PIC 9 VALUE 0.

       *> Zero-terminated (wf-name is "    ")
       01 workflows OCCURS 1000 TIMES INDEXED BY workflow-index.
           02 wf-name PIC X(3) VALUE "   ".
           *> Zero-terminated (category is " ")
           02 rules OCCURS 5 TIMES INDEXED BY rule-index.
               03 category PIC X VALUE " ".
               03 is-greater PIC 9.
               03 threshold PIC 9(4) COMP VALUE 0.
               03 dest PIC X(3) VALUE "   ".
           02 wf-default PIC X(3) VALUE "   ".
       
       *> x m a s
       01 stack OCCURS 1000 TIMES INDEXED BY stack-index.
           02 st-workflow-index PIC 9(3) COMP.
           02 st-rule-index     PIC 9(3) COMP.
           02 intervals OCCURS 4 TIMES INDEXED BY interval-index.
               *> Excluded
               03 int-start PIC 9(4) COMP.
               *> Included
               03 int-end   PIC 9(4) COMP.
       77 interval-index-2 INDEX.
       
       77 start-index INDEX.
       77 current-workflow-name PIC X(4).
       77 result PIC 9(16) COMP VALUE 0.
       
       77 temp-index INDEX.
       77 temp-n PIC 9(16) COMP.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
       
       MOVE 1 TO workflow-index
       PERFORM UNTIL end-of-file = 1
           IF current-line(1:1) = " "
               MOVE 1 TO end-of-file
           ELSE
               PERFORM parse-workflow
               ADD 1 TO workflow-index
           END-IF
           READ file-fd
       END-PERFORM
       CLOSE file-fd
       
       MOVE 1 TO stack-index
       MOVE start-index TO st-workflow-index(1)
       MOVE 1 TO st-rule-index(1)
       PERFORM VARYING interval-index FROM 1 BY 1 UNTIL interval-index > 4
           MOVE 0 TO int-start(1,interval-index)
           MOVE 4000 TO int-end(1,interval-index)
       END-PERFORM

       PERFORM UNTIL stack-index = 0
           PERFORM process-top-stack
       END-PERFORM

       DISPLAY "result = " result
       
       STOP RUN.

       parse-workflow.
           MOVE 1 TO parse-index
           PERFORM UNTIL current-line(parse-index:1) = "{"
               MOVE current-line(parse-index:1) TO wf-name(workflow-index) (parse-index:1)
               ADD 1 TO parse-index
           END-PERFORM
           IF wf-name(workflow-index) = "in"
               MOVE workflow-index TO start-index
           END-IF
           MOVE 1 TO rule-index
           PERFORM UNTIL current-line(parse-index:1) = "}"
               ADD 1 TO parse-index
               MOVE current-line(parse-index:1) TO category(workflow-index,rule-index)
               ADD 1 TO parse-index
               IF (current-line(parse-index:1) = "<") OR (current-line(parse-index:1) = ">")
                   IF current-line(parse-index:1) = "<"
                       MOVE 0 TO is-greater(workflow-index,rule-index)
                   ELSE
                       MOVE 1 TO is-greater(workflow-index,rule-index)
                   END-IF
                   ADD 1 TO parse-index
                   PERFORM UNTIL current-line(parse-index:1) = ":"
                       MOVE current-line(parse-index:1) TO char-as-int
                       MULTIPLY 10 BY threshold(workflow-index,rule-index)
                       ADD char-as-int TO threshold(workflow-index,rule-index)
                       ADD 1 TO parse-index
                   END-PERFORM
                   ADD 1 TO parse-index
                   MOVE 1 TO temp-index
                   PERFORM UNTIL current-line(parse-index:1) = ","
                       MOVE current-line(parse-index:1) TO dest(workflow-index,rule-index) (temp-index:1)
                       ADD 1 TO parse-index
                       ADD 1 TO temp-index
                   END-PERFORM
                   ADD 1 TO rule-index
               ELSE
                   MOVE category(workflow-index,rule-index) TO wf-default(workflow-index) (1:1)
                   MOVE " " TO category(workflow-index,rule-index)
                   MOVE 2 TO temp-index
                   PERFORM UNTIL current-line(parse-index:1) = "}"
                       MOVE current-line(parse-index:1) TO wf-default(workflow-index) (temp-index:1)
                       ADD 1 TO parse-index
                       ADD 1 TO temp-index
                   END-PERFORM
               END-IF
           END-PERFORM
           .
       
       process-top-stack.
           MOVE st-workflow-index(stack-index) TO workflow-index
           MOVE st-rule-index(stack-index) TO rule-index
           IF category(workflow-index,rule-index) = " "
               MOVE wf-default(workflow-index) TO current-workflow-name
               PERFORM next-workflow
           ELSE
               EVALUATE category(workflow-index,rule-index)
                WHEN "x" MOVE 1 TO interval-index
                WHEN "m" MOVE 2 TO interval-index
                WHEN "a" MOVE 3 TO interval-index
                WHEN "s" MOVE 4 TO interval-index
               END-EVALUATE
               IF is-greater(workflow-index,rule-index) = 0
                   *> all the interval is rejected
                   IF int-start(stack-index,interval-index) >= threshold(workflow-index,rule-index) - 1
                       ADD 1 TO st-rule-index(stack-index)
                   ELSE
                       *> all the interval is accepted
                       IF int-end(stack-index,interval-index) <= threshold(workflow-index,rule-index) - 1
                           MOVE dest(workflow-index,rule-index) TO current-workflow-name
                           PERFORM next-workflow
                       ELSE
                           *> We need to split the interval, add a new element to the stack
                           PERFORM duplicate-stack-top
                           ADD 1 TO st-rule-index(stack-index + 1)
                           COMPUTE int-end(stack-index,interval-index) = threshold(workflow-index,rule-index) - 1
                           COMPUTE int-start(stack-index + 1,interval-index) = threshold(workflow-index,rule-index) - 1
                           ADD 1 TO stack-index
                       END-IF
                   END-IF
               ELSE
                   *> all the interval is rejected
                   IF int-end(stack-index,interval-index) <= threshold(workflow-index,rule-index)
                       ADD 1 TO st-rule-index(stack-index)
                   ELSE
                       *> all the interval is accepted
                       IF int-start(stack-index,interval-index) >= threshold(workflow-index,rule-index)
                           MOVE dest(workflow-index,rule-index) TO current-workflow-name
                           PERFORM next-workflow
                       ELSE
                           *> We need to split the interval, add a new element to the stack
                           PERFORM duplicate-stack-top
                           ADD 1 TO st-rule-index(stack-index + 1)
                           MOVE threshold(workflow-index,rule-index) TO int-start(stack-index,interval-index)
                           MOVE threshold(workflow-index,rule-index) TO int-end(stack-index + 1,interval-index)
                           ADD 1 TO stack-index
                       END-IF
                   END-IF
               END-IF
           END-IF
           .
       
       duplicate-stack-top.
           PERFORM VARYING interval-index-2 FROM 1 BY 1 UNTIL interval-index-2 > 4
               MOVE int-start(stack-index,interval-index-2) TO int-start(stack-index + 1,interval-index-2)
               MOVE int-end(stack-index,interval-index-2) TO int-end(stack-index + 1,interval-index-2)
           END-PERFORM
           MOVE st-workflow-index(stack-index) TO st-workflow-index(stack-index + 1)
           MOVE st-rule-index(stack-index) TO st-rule-index(stack-index + 1)
           .
       
       next-workflow.
           IF current-workflow-name = "A"
               MOVE 1 TO temp-n
               PERFORM VARYING interval-index FROM 1 BY 1 UNTIL interval-index > 4
                   COMPUTE temp-n = temp-n * (int-end(stack-index,interval-index) - int-start(stack-index,interval-index))
               END-PERFORM
               ADD temp-n TO result
               SUBTRACT 1 FROM stack-index
           ELSE
               IF current-workflow-name = "R"
                   SUBTRACT 1 FROM stack-index
               ELSE
                   PERFORM search-workflow
                   MOVE workflow-index TO st-workflow-index(stack-index)
                   MOVE 1 TO st-rule-index(stack-index)
               END-IF
           END-IF
           .
       
       search-workflow.
           PERFORM VARYING workflow-index FROM 1 BY 1 UNTIL wf-name(workflow-index) = current-workflow-name
               CONTINUE
           END-PERFORM
           .
END PROGRAM AOC-DAY-19.
