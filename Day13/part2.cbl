IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-13.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day13/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(100).

       WORKING-STORAGE SECTION.
       REPLACE ==MaxSize== BY ==50==.
       77 line-data        PIC 9(16) OCCURS 1 TO MaxSize TIMES DEPENDING ON line-size INDEXED BY line-index USAGE COMPUTATIONAL.
       77 line-size        USAGE INDEX.
       77 col-data         PIC 9(16) OCCURS 1 TO MaxSize TIMES DEPENDING ON col-size INDEXED BY col-index USAGE COMPUTATIONAL.
       77 col-size         USAGE INDEX.

       77 found-reflection USAGE INDEX.
       77 array            PIC 9(16) OCCURS 1 TO MaxSize TIMES DEPENDING ON array-size INDEXED BY array-index USAGE COMPUTATIONAL.
       77 array-size       USAGE INDEX.
       77 forward-index    USAGE INDEX.
       77 backward-index   USAGE INDEX.
       77 found-smudge     PIC 9.

       *> 0 if the numbers are equal
       *> 1 if the have 1 bit of difference
       *> 2 else
       77 differ-by-one    PIC 9.

       77 result           PIC 9(16) USAGE COMPUTATIONAL.
       
       77 end-of-file      PIC 9 VALUE 0.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           PERFORM process-pattern
           *> Read the start of the next pattern, EOF if no more patterns are present.
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       
       CLOSE file-fd

       DISPLAY "result = " result
              
       STOP RUN.

       process-pattern.
           PERFORM VARYING col-index FROM 1 BY 1 UNTIL col-index > MaxSize
               MOVE 0 TO col-data(col-index)
           END-PERFORM
           MOVE 0 TO col-size
           MOVE 0 TO line-size
           PERFORM UNTIL current-line(1:1) = " "
               ADD 1 TO line-size
               MOVE 0 TO line-data(line-size)
               PERFORM VARYING col-index FROM 1 BY 1 UNTIL current-line(col-index:1) = " "
                   MOVE col-index TO col-size
                   MULTIPLY 2 BY line-data(line-size)
                   MULTIPLY 2 BY col-data(col-index)
                   IF current-line(col-index:1) = "#"
                       ADD 1 TO line-data(line-size)
                       ADD 1 TO col-data(col-index)
                   END-IF
               END-PERFORM
               READ file-fd
           END-PERFORM

           PERFORM analyze-pattern
           .
       
       analyze-pattern.
           *> analyze line
           PERFORM VARYING line-index FROM 1 BY 1 UNTIL line-index > line-size
               MOVE line-data(line-index) TO array(line-index)
           END-PERFORM
           MOVE line-size TO array-size
           PERFORM find-reflection
           IF found-reflection = 0
                *> analyze column
               PERFORM VARYING col-index FROM 1 BY 1 UNTIL col-index > col-size
                   MOVE col-data(col-index) TO array(col-index)
               END-PERFORM
               MOVE col-size TO array-size
               PERFORM find-reflection
               DISPLAY "column reflection at " found-reflection
               ADD found-reflection TO result
           ELSE
               DISPLAY "line reflection at " found-reflection
               COMPUTE result = result + 100 * found-reflection
           END-IF
           .
       
       find-reflection.
           MOVE 0 TO found-reflection
           PERFORM VARYING array-index FROM 1 BY 1 UNTIL array-index > array-size - 1
               MOVE 0 TO found-smudge
               COMPUTE forward-index = array-index + 1
               COMPUTE backward-index = array-index
               MOVE array-index TO found-reflection
               PERFORM UNTIL (forward-index > array-size) OR (backward-index = 0)
                   CALL 'DETERMINE-DIFFERENCE' USING BY CONTENT array(forward-index), array(backward-index), BY REFERENCE differ-by-one
                   IF differ-by-one > 0
                       IF found-smudge = 1
                           MOVE 0 TO found-reflection
                       ELSE
                           IF differ-by-one = 1
                               MOVE 1 TO found-smudge
                           ELSE
                               MOVE 0 TO found-reflection
                           END-IF
                       END-IF
                   END-IF
                   ADD 1 TO forward-index
                   SUBTRACT 1 FROM backward-index
               END-PERFORM
               IF found-smudge = 0
                   MOVE 0 TO found-reflection
               END-IF
               IF NOT found-reflection = 0
                   MOVE array-size TO array-index
               END-IF
           END-PERFORM
           .
END PROGRAM AOC-DAY-13.

IDENTIFICATION DIVISION.
       PROGRAM-ID.    DETERMINE-DIFFERENCE.
DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 one-bit-diff     PIC 9.

       LINKAGE SECTION.
       77 n1               PIC S9(16) USAGE COMPUTATIONAL.
       77 n2               PIC S9(16) USAGE COMPUTATIONAL.
       *> 0 if the numbers are equal
       *> 1 if the have 1 bit of difference
       *> 2 else
       77 differ-by-one    PIC 9.
PROCEDURE DIVISION USING n1, n2, differ-by-one.
       MOVE 0 TO differ-by-one
       PERFORM UNTIL (n1 = 0) AND (n2 = 0)
           COMPUTE one-bit-diff = FUNCTION MOD (n1, 2) + FUNCTION MOD (n2, 2)
           IF one-bit-diff = 1
               ADD 1 TO differ-by-one
           END-IF
           IF differ-by-one = 2
               MOVE 0 TO n1
               MOVE 0 TO n2
           END-IF
           DIVIDE 2 INTO n1
           DIVIDE 2 INTO n2
       END-PERFORM       
       EXIT PROGRAM.
END PROGRAM DETERMINE-DIFFERENCE.
