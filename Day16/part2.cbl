IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-16.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day16/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       REPLACE ==LineSize== BY ==110==.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(LineSize).

       WORKING-STORAGE SECTION.
       77 tiles             PIC X(LineSize) OCCURS LineSize TIMES INDEXED BY tile-line.
       01 FILLER OCCURS LineSize TIMES.
           02 FILLER OCCURS LineSize TIMES.
               03 energized-tiles PIC 9.
               03 energy-directions PIC X(4).
       77 energy-index      PIC 9 USAGE COMPUTATIONAL.
       77 tile-is-energized PIC 9.
       77 tile-col USAGE INDEX.

       01 beams-stack OCCURS 200 TIMES INDEXED BY beam-nb.
           02 beam-direction PIC X.
           02 beam-line      USAGE INDEX.
           02 beam-col       USAGE INDEX.
       77 temp-bool PIC 9.
       77 start-run-index USAGE INDEX.
       77 run-result PIC 9(16) USAGE COMPUTATIONAL.
       77 result     PIC 9(16) USAGE COMPUTATIONAL.

       77 end-of-file PIC 9 VALUE 0.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       MOVE 1 TO tile-line
       PERFORM UNTIL end-of-file = 1
           MOVE current-line TO tiles(tile-line)
           ADD 1 TO tile-line
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       CLOSE file-fd
       
       MOVE 0 TO result
       *> Left side
       PERFORM VARYING start-run-index FROM 1 BY 1 UNTIL start-run-index > LineSize
           MOVE 1 TO beam-nb
           MOVE "R" TO beam-direction(1)
           MOVE start-run-index TO beam-line(1)
           MOVE 0 TO beam-col(1)
           PERFORM make-run
           COMPUTE result = FUNCTION MAX(result, run-result)
       END-PERFORM
       *> Right side
       PERFORM VARYING start-run-index FROM 1 BY 1 UNTIL start-run-index > LineSize
           MOVE 1 TO beam-nb
           MOVE "L" TO beam-direction(1)
           MOVE start-run-index TO beam-line(1)
           COMPUTE beam-col(1) = LineSize + 1
           PERFORM make-run
           COMPUTE result = FUNCTION MAX(result, run-result)
       END-PERFORM
       *> Bottom side
       PERFORM VARYING start-run-index FROM 1 BY 1 UNTIL start-run-index > LineSize
           MOVE 1 TO beam-nb
           MOVE "T" TO beam-direction(1)
           COMPUTE  beam-line(1) = LineSize + 1
           MOVE start-run-index TO beam-col(1)
           PERFORM make-run
           COMPUTE result = FUNCTION MAX(result, run-result)
       END-PERFORM
       *> Top side
       PERFORM VARYING start-run-index FROM 1 BY 1 UNTIL start-run-index > LineSize
           MOVE 1 TO beam-nb
           MOVE "B" TO beam-direction(1)
           MOVE 0 TO beam-line(1)
           MOVE start-run-index TO beam-col(1)
           PERFORM make-run
           COMPUTE result = FUNCTION MAX(result, run-result)
       END-PERFORM

       DISPLAY "result = " result

       STOP RUN.
       
       *> Assume there is one beam entering the tiles (already set)
       make-run.
           *> Reset loop detection
           PERFORM VARYING tile-line FROM 1 BY 1 UNTIL tile-line > LineSize
               PERFORM VARYING tile-col FROM 1 BY 1 UNTIL tile-col > LineSize
                   MOVE 0 TO energized-tiles(tile-line,tile-col)
                   MOVE SPACES TO energy-directions(tile-line,tile-col)
               END-PERFORM
           END-PERFORM

           PERFORM UNTIL beam-nb = 0
               PERFORM advance-beam
           END-PERFORM

           MOVE 0 TO run-result
           PERFORM VARYING tile-line FROM 1 BY 1 UNTIL tile-line > LineSize
               PERFORM VARYING tile-col FROM 1 BY 1 UNTIL tile-col > LineSize
                   ADD energized-tiles(tile-line,tile-col) TO run-result
               END-PERFORM
           END-PERFORM
           .

       advance-beam.
           EVALUATE beam-direction(beam-nb)
            WHEN "R" ADD 1 TO beam-col(beam-nb)
            WHEN "L" SUBTRACT 1 FROM beam-col(beam-nb)
            WHEN "B" ADD 1 TO beam-line(beam-nb)
            WHEN "T" SUBTRACT 1 FROM beam-line(beam-nb)
           END-EVALUATE
           MOVE beam-line(beam-nb) TO tile-line
           MOVE beam-col(beam-nb) TO tile-col
           IF (tile-line = 0 OR LineSize + 1) OR ((tile-col = 0 OR LineSize + 1))
               *> The beam got out of the grid
               SUBTRACT 1 FROM beam-nb
           ELSE
               MOVE 0 TO tile-is-energized
               IF energized-tiles(tile-line,tile-col) = 1
                   PERFORM VARYING energy-index FROM 1 BY 1 UNTIL energy-index > 4
                       IF energy-directions(tile-line,tile-col) (energy-index:1) = beam-direction(beam-nb)
                           MOVE 1 TO tile-is-energized
                       END-IF
                   END-PERFORM
               END-IF
               IF tile-is-energized = 1
                   *> The beam is redundant
                   SUBTRACT 1 FROM beam-nb
               ELSE
                   *> Advance the beam !
                   MOVE 1 TO energized-tiles(tile-line,tile-col)
                   PERFORM VARYING energy-index FROM 1 BY 1 UNTIL energy-index > 4
                       IF energy-directions(tile-line,tile-col) (energy-index:1) = " "
                           MOVE beam-direction(beam-nb) TO energy-directions(tile-line,tile-col) (energy-index:1)
                           MOVE 5 TO energy-index
                       END-IF
                   END-PERFORM
                   
                   EVALUATE tiles(tile-line) (tile-col:1)
                    WHEN "." CONTINUE
                    WHEN "\"
                       EVALUATE beam-direction(beam-nb)
                        WHEN "R" MOVE "B" TO beam-direction(beam-nb)
                        WHEN "L" MOVE "T" TO beam-direction(beam-nb)
                        WHEN "B" MOVE "R" TO beam-direction(beam-nb)
                        WHEN "T" MOVE "L" TO beam-direction(beam-nb)
                       END-EVALUATE
                    WHEN "/"
                       EVALUATE beam-direction(beam-nb)
                        WHEN "R" MOVE "T" TO beam-direction(beam-nb)
                        WHEN "L" MOVE "B" TO beam-direction(beam-nb)
                        WHEN "B" MOVE "L" TO beam-direction(beam-nb)
                        WHEN "T" MOVE "R" TO beam-direction(beam-nb)
                       END-EVALUATE
                    WHEN "-"
                       EVALUATE beam-direction(beam-nb)
                        WHEN "B" MOVE 1 TO temp-bool
                        WHEN "T" MOVE 1 TO temp-bool
                        WHEN OTHER MOVE 0 TO temp-bool
                       END-EVALUATE
                       IF temp-bool = 1
                           MOVE "R" TO beam-direction(beam-nb)
                           ADD 1 TO beam-nb
                           MOVE "L" TO beam-direction(beam-nb)
                           MOVE beam-line(beam-nb - 1) TO beam-line(beam-nb)
                           MOVE beam-col(beam-nb - 1)  TO beam-col(beam-nb)
                       END-IF
                    WHEN "|"
                       EVALUATE beam-direction(beam-nb)
                        WHEN "R" MOVE 1 TO temp-bool
                        WHEN "L" MOVE 1 TO temp-bool
                        WHEN OTHER MOVE 0 TO temp-bool
                       END-EVALUATE
                       IF temp-bool = 1
                           MOVE "T" TO beam-direction(beam-nb)
                           ADD 1 TO beam-nb
                           MOVE "B" TO beam-direction(beam-nb)
                           MOVE beam-line(beam-nb - 1) TO beam-line(beam-nb)
                           MOVE beam-col(beam-nb - 1)  TO beam-col(beam-nb)
                       END-IF
                   END-EVALUATE
               END-IF
           END-IF
           .
END PROGRAM AOC-DAY-16.
