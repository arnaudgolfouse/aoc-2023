SUBDIRS := $(wildcard Day*/)
SOURCES := $(wildcard $(addsuffix *.cbl,$(SUBDIRS)))
EXECS := $(patsubst %.cbl,%,$(SOURCES))
FLAGS := -free -x -Wall

.PHONY: all clean

all: $(EXECS)

release: FLAGS += -O2
release: $(EXECS)

% : %.cbl
	cobc $(FLAGS) -o $@ $@.cbl

clean:
	rm -f */part1
	rm -f */part2
