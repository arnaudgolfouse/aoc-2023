IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-15.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day15/input.txt"
            ORGANIZATION IS SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-char PIC X.

       WORKING-STORAGE SECTION.
       77 end-of-file PIC 9 VALUE 0.
       77 hash-value PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
       77 result PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           IF current-char = ","
               ADD hash-value TO result
               MOVE 0 TO hash-value
           ELSE
               COMPUTE hash-value = FUNCTION MOD (17 * (hash-value + FUNCTION ORD (current-char) - 1), 256)
           END-IF
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM

       ADD hash-value TO result

       CLOSE file-fd
       
       DISPLAY "result = " result

       STOP RUN.
END PROGRAM AOC-DAY-15.
