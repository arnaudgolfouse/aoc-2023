
IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-5.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day5/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line   PIC X(300).

       WORKING-STORAGE SECTION.
       77 seed-ranges-nb  PIC 9(12).
       01 seed-ranges OCCURS 1000 TIMES INDEXED BY seeds-index.
           02 seed-range-start PIC 9(12).
           02 seed-range-len   PIC 9(12).
       77 current-seed-range-end PIC 9(12) USAGE COMPUTATIONAL.
       77 seeds-processed PIC 9     OCCURS 1000 TIMES.
       77 convert-range   PIC 9(12) OCCURS 03 TIMES INDEXED BY range-index.
       77 source-range    PIC 9(12) USAGE COMPUTATIONAL.
       77 dest-range      PIC 9(12) USAGE COMPUTATIONAL.
       77 range-length    PIC 9(12) USAGE COMPUTATIONAL.
       77 range-end       PIC 9(12) USAGE COMPUTATIONAL.
       77 range-diff      PIC 9(12) USAGE COMPUTATIONAL.
       77 range-decrease  PIC 9     USAGE COMPUTATIONAL.
       77 lowest-location PIC 9(10) USAGE COMPUTATIONAL.

       *> parsing variables
       77 parse-position  PIC 999   USAGE COMPUTATIONAL.
       77 char-as-int     PIC S9    USAGE COMPUTATIONAL.
       77 parsed-number   PIC 9(12) USAGE COMPUTATIONAL.
       77 end-of-file     PIC 9     USAGE COMPUTATIONAL.
       77 end-of-section  PIC 9     USAGE COMPUTATIONAL.

       *> temporary variables, used in computations

       77 num1            PIC 9(12) USAGE COMPUTATIONAL.
       77 num2            PIC 9(12) USAGE COMPUTATIONAL.
       77 num3            PIC 9(12) USAGE COMPUTATIONAL.

PROCEDURE DIVISION.
       MOVE 0 TO end-of-file
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM initialize-seeds.
       
       READ file-fd *> newline

       PERFORM UNTIL end-of-file = 1
           READ file-fd *> skip the section title

           MOVE 0 TO end-of-section
           MOVE 1 TO seeds-index
           PERFORM 1000 TIMES
               MOVE 0 TO seeds-processed(seeds-index)
               ADD 1 TO seeds-index
           END-PERFORM
           READ file-fd
           PERFORM read-range
           PERFORM UNTIL end-of-section = 1
               *> main logic
               MOVE 1 TO seeds-index
               PERFORM UNTIL seeds-index > seed-ranges-nb
                   IF seeds-processed(seeds-index) = 0
                       MOVE seed-range-start(seeds-index) TO current-seed-range-end
                       ADD  seed-range-len(seeds-index) TO current-seed-range-end

                       IF (seed-range-start(seeds-index) >= source-range) AND (current-seed-range-end <= range-end)
                           *> Case 1: the range is fully into the conversion range
                           MOVE 1 TO seeds-processed(seeds-index)
                           IF range-decrease = 0
                               ADD range-diff      TO   seed-range-start(seeds-index)
                           ELSE
                               SUBTRACT range-diff FROM seed-range-start(seeds-index)
                           END-IF
                       END-IF
                       IF (seeds-processed(seeds-index) = 0) AND (seed-range-start(seeds-index) >= source-range) AND (seed-range-start(seeds-index) < range-end)
                           *> Case 2: the range starts into the conversion range
                           MOVE 1 TO seeds-processed(seeds-index)
                           MOVE range-end TO num1 *> first part's length
                           SUBTRACT seed-range-start(seeds-index) FROM num1
                           MOVE current-seed-range-end TO num2 *> second part's length
                           SUBTRACT range-end FROM num2

                           ADD 1 TO seed-ranges-nb

                           MOVE seed-range-start(seeds-index) TO seed-range-start(seed-ranges-nb)
                           ADD num1 TO seed-range-start(seed-ranges-nb)
                           MOVE num2 TO seed-range-len(seed-ranges-nb)
                           MOVE 0 TO seeds-processed(seed-ranges-nb)

                           IF range-decrease = 0
                               ADD range-diff      TO   seed-range-start(seeds-index)
                           ELSE
                               SUBTRACT range-diff FROM seed-range-start(seeds-index)
                           END-IF
                           MOVE num1 TO seed-range-len(seeds-index)
                       END-IF
                       IF (seeds-processed(seeds-index) = 0) AND (current-seed-range-end > source-range) AND (current-seed-range-end <= range-end)
                           *> Case 3: the range ends into the conversion range
                           MOVE 1 TO seeds-processed(seeds-index)
                           MOVE source-range TO num1 *> first part's length
                           SUBTRACT seed-range-start(seeds-index) FROM num1
                           MOVE current-seed-range-end TO num2 *> second part's length
                           SUBTRACT source-range FROM num2

                           ADD 1 TO seed-ranges-nb

                           MOVE seed-range-start(seeds-index) TO seed-range-start(seed-ranges-nb)
                           MOVE num1 TO seed-range-len(seed-ranges-nb)
                           MOVE 0 TO seeds-processed(seed-ranges-nb)

                           ADD num1 TO seed-range-start(seeds-index)
                           IF range-decrease = 0
                               ADD range-diff      TO   seed-range-start(seeds-index)
                           ELSE
                               SUBTRACT range-diff FROM seed-range-start(seeds-index)
                           END-IF
                           MOVE num2 TO seed-range-len(seeds-index)
                       END-IF
                       IF (seeds-processed(seeds-index) = 0) AND (seed-range-start(seeds-index) < source-range) AND (current-seed-range-end > range-end)
                           *> Case 4: the range contains the conversion range
                           MOVE 1 TO seeds-processed(seeds-index)
                           MOVE source-range TO num1 *> first part's length
                           SUBTRACT seed-range-start(seeds-index) FROM num1
                           MOVE range-length TO num2 *> second part's length
                           MOVE current-seed-range-end TO num3 *> third part's length
                           SUBTRACT range-end FROM num3

                           ADD 1 TO seed-ranges-nb

                           MOVE seed-range-start(seeds-index) TO seed-range-start(seed-ranges-nb)
                           MOVE num1 TO seed-range-len(seed-ranges-nb)
                           MOVE 0 TO seeds-processed(seed-ranges-nb)

                           ADD 1 TO seed-ranges-nb
                           MOVE range-end TO seed-range-start(seed-ranges-nb)
                           MOVE num3 TO seed-range-len(seed-ranges-nb)
                           MOVE 0 TO seeds-processed(seed-ranges-nb)

                           ADD num1 TO seed-range-start(seeds-index)
                           IF range-decrease = 0
                               ADD range-diff      TO   seed-range-start(seeds-index)
                           ELSE
                               SUBTRACT range-diff FROM seed-range-start(seeds-index)
                           END-IF
                           MOVE num2 TO seed-range-len(seeds-index)
                       END-IF
                   END-IF
                   ADD 1 TO seeds-index
               END-PERFORM

               *> Read the next line
               READ file-fd
                   AT END MOVE 1 TO end-of-file
               END-READ
               IF end-of-file = 0
                   PERFORM read-range
               ELSE
                   MOVE 1 TO end-of-section
               END-IF
           END-PERFORM
       END-PERFORM

       MOVE 1 TO seeds-index
       MOVE seed-range-start(1) TO lowest-location
       PERFORM UNTIL seeds-index > seed-ranges-nb
           IF seed-range-start(seeds-index) < lowest-location
               MOVE seed-range-start(seeds-index) TO lowest-location
           END-IF
           ADD 1 TO seeds-index
       END-PERFORM
       
       DISPLAY "lowest-location = " lowest-location
       
       CLOSE file-fd
       STOP RUN.

       initialize-seeds.
           *> Start after "seeds: "
           MOVE 0 TO num1
           MOVE 8 TO parse-position
           MOVE 1 TO seeds-index
           PERFORM UNTIL num1 = 1
               PERFORM parse-number
               IF parsed-number = 0
                   MOVE 1 TO num1
               ELSE
                   ADD 1 TO parse-position *> skip the space
                   MOVE parsed-number TO seed-range-start(seeds-index)
    
                   PERFORM parse-number
                   ADD 1 TO parse-position *> skip the space
                   MOVE parsed-number TO seed-range-len(seeds-index)
    
                   ADD 1 TO seeds-index
               END-IF
           END-PERFORM
           MOVE seeds-index TO seed-ranges-nb
           SUBTRACT 1 FROM seed-ranges-nb
           .
       
       read-range.
           MOVE 1 TO parse-position
           MOVE 1 TO range-index
           PERFORM 3 TIMES
               PERFORM parse-number
               MOVE parsed-number TO convert-range(range-index)
               ADD 1 TO range-index
               IF NOT (parse-position = 1)
                   ADD 1 TO parse-position
               END-IF
           END-PERFORM
           IF (parse-position = 1)
               MOVE 1 TO end-of-section
           ELSE
               MOVE convert-range(1) TO dest-range
               MOVE convert-range(2) TO source-range
               MOVE convert-range(3) TO range-length
               MOVE source-range     TO range-end
               ADD  range-length     TO range-end
               IF source-range < dest-range
                   MOVE dest-range       TO range-diff
                   SUBTRACT source-range FROM range-diff
                   MOVE 0 TO range-decrease
               ELSE
                   MOVE source-range   TO range-diff
                   SUBTRACT dest-range FROM range-diff
                   MOVE 1 TO range-decrease
               END-IF 
           END-IF
           .
       
       *> Parse a number in the current line, and put the result in `parsed-number`.
       *> After this function, `parse-position` will be right after the parsed number.
       parse-number.
           MOVE 0 TO parsed-number
           MOVE current-line(parse-position:1) TO char-as-int
           PERFORM UNTIL (NOT (current-line(parse-position:1) = "0")) AND (char-as-int = 0)
               MULTIPLY 10 BY parsed-number
               ADD char-as-int TO parsed-number
               ADD 1 TO parse-position
               MOVE current-line(parse-position:1) TO char-as-int
           END-PERFORM
           .
END PROGRAM AOC-DAY-5.
