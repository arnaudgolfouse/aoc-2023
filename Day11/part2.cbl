IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-11.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day11/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       REPLACE ==LineLength== BY ==140==.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(LineLength).

       WORKING-STORAGE SECTION.
       77 map PIC X(LineLength) OCCURS LineLength TIMES.
       77 map-line  USAGE INDEX.
       77 map-col   USAGE INDEX.
       77 is-expanded-line PIC 9 OCCURS LineLength TIMES USAGE COMPUTATIONAL.
       77 is-expanded-col PIC 9 OCCURS LineLength TIMES USAGE COMPUTATIONAL.
       01 galaxies OCCURS 500 TIMES INDEXED BY galaxy-index.
           02 galaxy-line PIC 999.
           02 galaxy-col  PIC 999.
       77 galaxy-index-2 USAGE INDEX.
       77 pos-from       PIC 999 USAGE COMPUTATIONAL.
       77 pos-to         PIC 999 USAGE COMPUTATIONAL.
       77 nb-of-galaxies USAGE INDEX.
       77 distance     PIC 9(16) USAGE COMPUTATIONAL.
       77 result       PIC 9(16) USAGE COMPUTATIONAL.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       PERFORM VARYING map-line FROM 1 BY 1 UNTIL map-line > LineLength
           READ file-fd
           MOVE current-line TO map(map-line)
       END-PERFORM
       CLOSE file-fd
       
       MOVE 0 TO nb-of-galaxies
       PERFORM VARYING map-line FROM 1 BY 1 UNTIL map-line > LineLength
           MOVE 1 TO is-expanded-line(map-line)
           PERFORM VARYING map-col FROM 1 BY 1 UNTIL map-col > LineLength
               IF map(map-line) (map-col:1) = "#"
                   MOVE 0 TO is-expanded-line(map-line)
                   ADD 1 TO nb-of-galaxies
                   MOVE map-line TO galaxy-line(nb-of-galaxies)
                   MOVE map-col TO galaxy-col(nb-of-galaxies)
               END-IF
           END-PERFORM
       END-PERFORM
       PERFORM VARYING map-col FROM 1 BY 1 UNTIL map-col > LineLength
           MOVE 1 TO is-expanded-col(map-col)
           PERFORM VARYING map-line FROM 1 BY 1 UNTIL map-line > LineLength
               IF map(map-line) (map-col:1) = "#"
                   MOVE 0 TO is-expanded-col(map-col)
               END-IF
           END-PERFORM
       END-PERFORM

       PERFORM VARYING galaxy-index FROM 1 BY 1 UNTIL galaxy-index > nb-of-galaxies
           COMPUTE galaxy-index-2 = galaxy-index + 1
           PERFORM UNTIL galaxy-index-2 > nb-of-galaxies
               MOVE 2 TO distance
               IF galaxy-line(galaxy-index) = galaxy-line(galaxy-index-2)
                   SUBTRACT 1 FROM distance
               END-IF
               IF galaxy-col(galaxy-index) = galaxy-col(galaxy-index-2)
                   SUBTRACT 1 FROM distance
               END-IF
               IF galaxy-line(galaxy-index) < galaxy-line(galaxy-index-2)
                   MOVE galaxy-line(galaxy-index) TO pos-from
                   MOVE galaxy-line(galaxy-index-2) TO pos-to
               ELSE
                   MOVE galaxy-line(galaxy-index-2) TO pos-from
                   MOVE galaxy-line(galaxy-index) TO pos-to
               END-IF
               COMPUTE map-line = pos-from + 1
               PERFORM UNTIL map-line >= pos-to
                   IF is-expanded-line(map-line) = 1
                       ADD 999999 TO distance
                   END-IF
                   ADD 1 TO distance
                   ADD 1 TO map-line
               END-PERFORM

               IF galaxy-col(galaxy-index) < galaxy-col(galaxy-index-2)
                   MOVE galaxy-col(galaxy-index) TO pos-from
                   MOVE galaxy-col(galaxy-index-2) TO pos-to
               ELSE
                   MOVE galaxy-col(galaxy-index-2) TO pos-from
                   MOVE galaxy-col(galaxy-index) TO pos-to
               END-IF
               COMPUTE map-col = pos-from + 1
               PERFORM UNTIL map-col >= pos-to
                   IF is-expanded-col(map-col) = 1
                       ADD 999999 TO distance
                   END-IF
                   ADD 1 TO distance
                   ADD 1 TO map-col
               END-PERFORM

               ADD distance TO result
               ADD 1 TO galaxy-index-2
           END-PERFORM
       END-PERFORM

       DISPLAY result
       
       STOP RUN.
END PROGRAM AOC-DAY-11.
