IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-18.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day18/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(30).

       WORKING-STORAGE SECTION.
       77 parse-index INDEX.
       77 current-direction PIC X.
       77 current-steps     PIC 9(16) COMPUTATIONAL VALUE 0.
       77 terrain-line  INDEX.
       77 terrain-col   INDEX.
       77 previous-line INDEX.
       77 previous-col  INDEX.
       77 char-as-int PIC 9.
       77 end-of-file PIC 9 VALUE 0.
       
       77 total-steps PIC S9(16)v99 COMPUTATIONAL VALUE 0.0.
       77 result      PIC S9(16)v99 COMPUTATIONAL VALUE 0.0.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ
       
       MOVE 0 TO terrain-line MOVE 0 TO terrain-col
       MOVE 0 TO previous-line MOVE 0 TO previous-col
       PERFORM UNTIL end-of-file = 1
           PERFORM parse-hexadecimal-code
           
           EVALUATE current-direction
            WHEN "R" ADD      current-steps TO   terrain-col
            WHEN "L" SUBTRACT current-steps FROM terrain-col
            WHEN "D" ADD      current-steps TO   terrain-line
            WHEN "U" SUBTRACT current-steps FROM terrain-line
           END-EVALUATE
       
           COMPUTE result = result + ((terrain-line + previous-line) * (previous-col - terrain-col)) / 2
           *> Adjustments for the contour
           ADD current-steps TO total-steps
           
           MOVE terrain-line       TO previous-line
           MOVE terrain-col        TO previous-col
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       CLOSE file-fd
       
       *> The 4 clockwise turns
       ADD 1 TO result
       *> Half of the contour is missing ! 
       COMPUTE result = result + (total-steps / 2)
       DISPLAY "result = " result
       
       STOP RUN.

       parse-hexadecimal-code.
           PERFORM VARYING parse-index FROM 1 BY 1 UNTIL current-line(parse-index:2) = "(#"
               CONTINUE
           END-PERFORM
           ADD 2 TO parse-index
           MOVE 0 TO current-steps
           PERFORM 5 TIMES
               MULTIPLY 16 BY current-steps
               EVALUATE current-line(parse-index:1)
                WHEN "a" ADD 10 TO current-steps
                WHEN "b" ADD 11 TO current-steps
                WHEN "c" ADD 12 TO current-steps
                WHEN "d" ADD 13 TO current-steps
                WHEN "e" ADD 14 TO current-steps
                WHEN "f" ADD 15 TO current-steps
                WHEN OTHER
                   MOVE current-line(parse-index:1) TO char-as-int
                   ADD char-as-int TO current-steps
                END-EVALUATE
                ADD 1 to parse-index
           END-PERFORM
           EVALUATE current-line(parse-index:1)
            WHEN "0" MOVE "R" TO current-direction
            WHEN "1" MOVE "D" TO current-direction
            WHEN "2" MOVE "L" TO current-direction
            WHEN "3" MOVE "U" TO current-direction
           END-EVALUATE
           .
END PROGRAM AOC-DAY-18.
