# Advent of Code 2023, in COBOL

The goal is to do at least 1 problem per week 😅

# Build

You need `gnucobol` and `make`. On Ubuntu: `sudo apt install gnucobol make`

Then, run `make -j`.