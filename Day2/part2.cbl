IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-2.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day2/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 game-line PIC X(200) VALUE SPACES.

       WORKING-STORAGE SECTION.
       77 parse-position PIC 999.
       77 parse-position-next PIC 999.
       77 char-as-int PIC 9.
       77 parsed-number PIC 999.
       77 parsed-color PIC 9.
       77 parsed-end-of-set PIC Z.
       77 parsed-end-of-game PIC Z.
       77 stop-bool PIC Z.
       77 end-of-file PIC Z
           VALUE 0.
       
       77 game-id PIC 999.
       77 max-red PIC 999.
       77 max-green PIC 999.
       77 max-blue PIC 999.
       77 game-power PIC 9(5).
       77 sum-of-powers PIC 9(8) VALUE ZEROES.

PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           *> DISPLAY game-line
           *> We skip "Game "
           MOVE 6 TO parse-position
           PERFORM parse-number
           DISPLAY "GAME no°" parsed-number
           MOVE parsed-number TO game-id
           
           MOVE 0 TO parsed-end-of-game
           MOVE 0 TO max-red
           MOVE 0 TO max-green
           MOVE 0 TO max-blue

           PERFORM UNTIL parsed-end-of-game = 1
               MOVE 0 TO parsed-end-of-set
               PERFORM UNTIL parsed-end-of-set = 1
                   *> skip ": " or ", "
                   ADD 2 TO parse-position
                   PERFORM parse-number
                   
                   *> skip " "
                   ADD 1 TO parse-position

                   MOVE 4 TO parsed-color
                   PERFORM parse-color
                   IF (parsed-color = 0) AND (parsed-number > max-red)
                       *> red
                       MOVE parsed-number TO max-red
                   END-IF
                   IF (parsed-color = 1) AND (parsed-number > max-green)
                       *> green
                       MOVE parsed-number TO max-green
                   END-IF
                   IF (parsed-color = 2) AND (parsed-number > max-blue)
                       *> blue
                       MOVE parsed-number TO max-blue
                   END-IF
                   IF parsed-color > 2
                       DISPLAY "ERROR at '" game-line(parse-position:5) "' : did not parse a color !"
                   END-IF
                   
                   *> end of the set
                   PERFORM is-set-ended
               END-PERFORM

               *> end of the line
               PERFORM is-game-ended
           END-PERFORM

           MOVE max-red TO game-power
           MULTIPLY max-green BY game-power
           MULTIPLY max-blue BY game-power

           ADD game-power TO sum-of-powers

           MOVE SPACES TO game-line
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       
       DISPLAY " "
       DISPLAY "TOTAL: " sum-of-powers

       CLOSE file-fd
       STOP RUN.

       parse-number.
           MOVE 0 TO parsed-number
           MOVE 0 TO stop-bool
           PERFORM UNTIL stop-bool = 1
               MOVE game-line(parse-position:1) TO char-as-int
               IF (game-line(parse-position:1) = "0") OR NOT (char-as-int = 0)
                   MULTIPLY 10 BY parsed-number
                   ADD char-as-int TO parsed-number
                   ADD 1 TO parse-position
               ELSE
                   MOVE 1 TO stop-bool
               END-IF
           END-PERFORM.
       
       parse-color.
           IF game-line(parse-position:3) = "red"
               MOVE 0 TO parsed-color
               ADD 3 TO parse-position
           ELSE IF game-line(parse-position:5) = "green"
               MOVE 1 TO parsed-color
               ADD 5 TO parse-position
           ELSE IF game-line(parse-position:4) = "blue"
               MOVE 2 TO parsed-color
               ADD 4 TO parse-position
           END-IF.
       
       is-set-ended.
           IF (game-line(parse-position:1) = ";") OR (game-line(parse-position:1) = " ")
               MOVE 1 TO parsed-end-of-set
           ELSE
               MOVE 0 TO parsed-end-of-set
           END-IF.

       is-game-ended.
           IF game-line(parse-position:1) = " "
               MOVE 1 TO parsed-end-of-game
           ELSE
               MOVE 0 TO parsed-end-of-game
           END-IF.
END PROGRAM AOC-DAY-2.
