IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-14.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day14/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       REPLACE ==LineSize== BY ==100==.
       FD file-fd.
       01 current-line PIC X(LineSize).

       WORKING-STORAGE SECTION.
       01 platform OCCURS LineSize TIMES INDEXED BY platform-line-idx.
           02 platform-line PIC X(LineSize).
       77 platform-col-idx USAGE INDEX.
       77 free-position USAGE INDEX.
       77 weight USAGE INDEX.
       
       77 result PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       PERFORM VARYING platform-line-idx FROM 1 BY 1 UNTIL platform-line-idx > LineSize
           READ file-fd
           MOVE current-line TO platform-line(platform-line-idx)
       END-PERFORM
       CLOSE file-fd

       PERFORM tilt-north

       MOVE LineSize TO weight
       PERFORM VARYING platform-line-idx FROM 1 BY 1 UNTIL platform-line-idx > LineSize
           PERFORM VARYING platform-col-idx FROM 1 BY 1 UNTIL platform-col-idx > LineSize
               IF platform-line(platform-line-idx) (platform-col-idx:1) = "O"
                   COMPUTE result = result + weight
               END-IF
           END-PERFORM
           SUBTRACT 1 FROM weight
       END-PERFORM
       
       DISPLAY "result = " result
              
       STOP RUN.

       tilt-north.
           PERFORM VARYING platform-col-idx FROM 1 BY 1 UNTIL platform-col-idx > LineSize
               MOVE 0 TO free-position
               PERFORM VARYING platform-line-idx FROM 1 BY 1 UNTIL platform-line-idx > LineSize
                   EVALUATE platform-line(platform-line-idx) (platform-col-idx:1)
                    WHEN "#" MOVE 0 TO free-position
                    WHEN "O"
                       IF NOT (free-position = 0)
                           MOVE "O" TO platform-line(free-position) (platform-col-idx:1)
                           MOVE "." TO platform-line(platform-line-idx) (platform-col-idx:1)
                           ADD 1 TO free-position
                       END-IF
                    WHEN "." 
                       IF free-position = 0
                           MOVE platform-line-idx TO free-position
                       END-IF
                   END-EVALUATE
               END-PERFORM
           END-PERFORM
           .
END PROGRAM AOC-DAY-14.
