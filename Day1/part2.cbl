IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-1.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day1/input.txt"
            ORGANIZATION IS SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-char PIC X.

       WORKING-STORAGE SECTION.
       77 char-as-int PIC 99.
       77 spelled-integer PIC X(100)
           VALUE SPACES.
       77 spelled-integer-offset PIC 99
           VALUE 0.
       77 spelled-integer-number PIC 99
           VALUE 0.
       77 spelled-integer-search-string PIC X(6).
       77 spelled-integer-found-occurence PIC 9.
       77 first-number PIC 9(2)
           VALUE 0.
       77 second-number PIC 9
           VALUE 0.
       77 total PIC 9(7)
           VALUE 0.
       77 end-of-file PIC Z
           VALUE 0.

PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           MOVE current-char TO char-as-int
           IF (current-char = "0") OR (char-as-int > 0)
               PERFORM move-current-int-to-numbers
           END-IF
           IF NOT (current-char = X"0A")
               *> alphabetic character, put it in `spelled-integer`
               ADD 1 TO spelled-integer-offset
               MOVE current-char TO spelled-integer(spelled-integer-offset:1)
               MOVE SPACES TO spelled-integer-search-string
               *> The order here is very important, to avoid searching e.g. "sixe" (the 'e' is from "five") 
               MOVE "one" to spelled-integer-search-string
               MOVE 1 TO spelled-integer-number
               PERFORM search-number-spelled
               MOVE "two" to spelled-integer-search-string
               MOVE 2 TO spelled-integer-number
               PERFORM search-number-spelled
               MOVE "six" to spelled-integer-search-string
               MOVE 6 TO spelled-integer-number
               PERFORM search-number-spelled
               MOVE "four" to spelled-integer-search-string
               MOVE 4 TO spelled-integer-number
               PERFORM search-number-spelled
               MOVE "five" to spelled-integer-search-string
               MOVE 5 TO spelled-integer-number
               PERFORM search-number-spelled
               MOVE "nine" to spelled-integer-search-string
               MOVE 9 TO spelled-integer-number
               PERFORM search-number-spelled
               MOVE "three" to spelled-integer-search-string
               MOVE 3 TO spelled-integer-number
               PERFORM search-number-spelled
               MOVE "seven" to spelled-integer-search-string
               MOVE 7 TO spelled-integer-number
               PERFORM search-number-spelled
               MOVE "eight" to spelled-integer-search-string
               MOVE 8 TO spelled-integer-number
               PERFORM search-number-spelled
           END-IF
           PERFORM check-newline
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       DISPLAY total
       CLOSE file-fd
       STOP RUN.

       search-number-spelled.
           MOVE 0 TO spelled-integer-found-occurence
           INSPECT spelled-integer TALLYING spelled-integer-found-occurence
               FOR ALL spelled-integer-search-string
           IF spelled-integer-found-occurence = 1
               MOVE spelled-integer-number TO char-as-int
               PERFORM move-current-int-to-numbers
           END-IF
           .

       move-current-int-to-numbers.
           IF first-number = 0
               MOVE char-as-int TO first-number
           ELSE
               MOVE char-as-int TO second-number
           END-IF.

       check-newline.
           *> If the currently read character is a newline, check the number for this line and add it to the total. 
           IF current-char = X"0A"
               MOVE SPACES TO spelled-integer
               MOVE 0 TO spelled-integer-offset
               IF second-number = 0
                   MOVE first-number TO second-number
               END-IF
               MULTIPLY 10 BY first-number
               ADD second-number TO first-number
               ADD first-number TO total
               MOVE 0 TO first-number
               MOVE 0 TO second-number
           END-IF.
END PROGRAM AOC-DAY-1.
