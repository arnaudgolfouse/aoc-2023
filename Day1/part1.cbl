IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-1.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day1/input.txt"
            ORGANIZATION IS SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-char PIC X.

       WORKING-STORAGE SECTION.
       77 char-as-int PIC 9.
       77 first-number PIC 9(2) VALUE 0.
       77 second-number PIC 9 VALUE 0.
       77 total PIC 9(7) VALUE 0.
       77 end-of-file PIC Z VALUE 0.

PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ
      
       PERFORM UNTIL end-of-file = 1
           MOVE current-char TO char-as-int
           IF (current-char = "0") OR (char-as-int > 0)
               IF first-number = 0
                   MOVE char-as-int TO first-number
               ELSE
                   MOVE char-as-int TO second-number
               END-IF
           END-IF
           PERFORM check-newline
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       DISPLAY total
       CLOSE file-fd
       STOP RUN.

       check-newline.
           *> If the currently read character is a newline, check the number for this line and add it to the total. 
           IF current-char = X"0A"
              IF second-number = 0
                  MOVE first-number TO second-number
              END-IF
              MULTIPLY 10 BY first-number
              ADD second-number TO first-number
              ADD first-number TO total
              MOVE 0 TO first-number
              MOVE 0 TO second-number
           END-IF.
END PROGRAM AOC-DAY-1.
