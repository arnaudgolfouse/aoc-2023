IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-8.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day8/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(300).

       WORKING-STORAGE SECTION.
       77 instructions PIC X(300) VALUE SPACES.
       77 step-index   PIC 999   USAGE COMPUTATIONAL.
       77 step-number  PIC 9(16) USAGE COMPUTATIONAL.
       01 nodes OCCURS 714 TIMES INDEXED BY node-index.
           02 start-node PIC X(3).
           02 left-node  PIC X(3).
           02 right-node PIC X(3).
       77 searched-node PIC X(3).

       77 end-of-file PIC 9.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       *> The instructions
       READ file-fd
       MOVE current-line TO instructions
       READ file-fd *> skip newline
       READ file-fd
       
       MOVE 1 TO node-index
       PERFORM UNTIL end-of-file = 1
           MOVE current-line(1:3) TO start-node(node-index)
           MOVE current-line(8:3) TO left-node(node-index)
           MOVE current-line(13:3) TO right-node(node-index)
           ADD 1 TO node-index
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       
       CLOSE file-fd

       MOVE "AAA" TO searched-node
       PERFORM search-node-index

       MOVE 1 TO step-index
       MOVE 0 TO step-number
       PERFORM UNTIL (start-node(node-index) = "ZZZ") AND (instructions(step-index:1) = " ")
           IF instructions(step-index:1) = " "
               MOVE 1 TO step-index
           END-IF
           IF instructions(step-index:1) = "L"
               MOVE left-node(node-index) TO searched-node
           ELSE
               MOVE right-node(node-index) TO searched-node
           END-IF
           PERFORM search-node-index
           ADD 1 TO step-index
           ADD 1 TO step-number
       END-PERFORM

       DISPLAY step-number
       
       STOP RUN.

       search-node-index.
           MOVE 1 TO node-index
           PERFORM UNTIL start-node(node-index) = searched-node
               ADD 1 TO node-index
           END-PERFORM
           .
END PROGRAM AOC-DAY-8.
