IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-12.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day12/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(200).

       WORKING-STORAGE SECTION.
       77 parse-position  USAGE INDEX.
       77 char-as-int     PIC 9.
       77 parsed-number   PIC 9(4) USAGE COMPUTATIONAL.

       77 current-data    PIC X(200).
       77 data-length     USAGE INDEX.
       77 data-index      USAGE INDEX.
       77 increased-data-index USAGE INDEX.
       77 current-numbers PIC 999 OCCURS 200 TIMES INDEXED BY numbers-index USAGE COMPUTATIONAL.
       77 numbers-index-2 USAGE INDEX.
       77 numbers-length  USAGE INDEX.
       77 numbers-total   PIC 999 USAGE COMPUTATIONAL.

       77 qmarks-positions PIC 999 OCCURS 200 TIMES INDEXED BY qmark-index USAGE COMPUTATIONAL.
       77 qmark-index-2   USAGE INDEX.
       77 increased-qmark USAGE INDEX.
       77 qmarks-nb       PIC 999 USAGE COMPUTATIONAL.
       77 pounds-nb       PIC 999 USAGE COMPUTATIONAL.
       77 additional-pounds PIC 9 OCCURS 200 TIMES USAGE COMPUTATIONAL.
       77 cannot-increase PIC 9.
       77 data-valid      PIC 9.
       77 nb-of-combinations PIC 9(16) USAGE COMPUTATIONAL.
       77 increase-at     USAGE INDEX.
       
       77 old-numbers-positions     PIC 999 OCCURS 50 TIMES USAGE COMPUTATIONAL.
       77 current-numbers-positions PIC 999 OCCURS 50 TIMES USAGE COMPUTATIONAL.
       01 memoization OCCURS 50 TIMES.
           *> memo-at(i,j) -> number of combinations if the i-th number is at index j.
           *> If the cell is not filled yet, it contains -1.
           *> If no further combination is valid, it contains 0
           02 memo-at PIC S9(16) OCCURS 200 TIMES USAGE COMPUTATIONAL.

       77 temp-n-1        PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
       77 temp-n-2        PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
       77 temp-bool-1     PIC 9.
       77 temp-bool-2     PIC 9.
       77 temp-display    PIC X(200).
       77 result          PIC 9(16) USAGE COMPUTATIONAL VALUE 0.

       77 end-of-file     PIC 9 VALUE 0.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           PERFORM process-line
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       
       CLOSE file-fd
       
       DISPLAY "result = " result
       
       STOP RUN.

       process-line.
           PERFORM parse-line

           MOVE 0 TO nb-of-combinations
           PERFORM VARYING numbers-index FROM 1 BY 1 UNTIL numbers-index > numbers-length
               PERFORM VARYING data-index FROM 1 BY 1 UNTIL data-index > data-length
                   MOVE -1 TO memo-at(numbers-index,data-index)
               END-PERFORM
           END-PERFORM
           
           *> expected number of '#' to fill
           COMPUTE temp-n-1 = numbers-total - pounds-nb
           PERFORM VARYING qmark-index FROM 1 BY 1 UNTIL qmark-index > qmarks-nb
               IF qmark-index <= temp-n-1
                   MOVE 1 TO additional-pounds(qmark-index)
               ELSE
                   MOVE 0 TO additional-pounds(qmark-index)
               END-IF
           END-PERFORM

           MOVE 0 TO cannot-increase
           MOVE 1 TO increased-qmark
           MOVE 1 TO increased-data-index
           PERFORM VARYING numbers-index FROM 1 BY 1 UNTIL numbers-index > numbers-length
               MOVE 1 TO current-numbers-positions(numbers-index)
               MOVE 1 TO old-numbers-positions(numbers-index)
           END-PERFORM

           PERFORM UNTIL cannot-increase = 1
               PERFORM is-current-data-valid

               IF data-valid = 1
                   IF increase-at > 0
                       MOVE memo-at(numbers-index,current-numbers-positions(numbers-index)) TO temp-n-1
                       ADD temp-n-1 TO nb-of-combinations
                       COMPUTE numbers-index = numbers-index - 1
                       PERFORM VARYING numbers-index FROM numbers-index BY -1 UNTIL numbers-index = 0
                           IF memo-at(numbers-index,current-numbers-positions(numbers-index)) >= 0
                               ADD temp-n-1 TO memo-at(numbers-index,current-numbers-positions(numbers-index))
                           ELSE
                               MOVE temp-n-1 TO memo-at(numbers-index,current-numbers-positions(numbers-index))
                           END-IF
                       END-PERFORM
                       PERFORM shortcircuit-increase-at
                   ELSE
                       ADD 1 TO nb-of-combinations
                       PERFORM VARYING numbers-index FROM 1 BY 1 UNTIL numbers-index > numbers-length
                           IF memo-at(numbers-index,current-numbers-positions(numbers-index)) >= 0
                               ADD 1 TO memo-at(numbers-index,current-numbers-positions(numbers-index))
                           ELSE
                               MOVE 1 TO memo-at(numbers-index,current-numbers-positions(numbers-index))
                           END-IF
                       END-PERFORM
                       PERFORM increase-additional-pounds
                   END-IF
               ELSE
                   COMPUTE numbers-index = numbers-index - 1
                   PERFORM VARYING numbers-index FROM numbers-index BY -1 UNTIL numbers-index = 0
                       IF memo-at(numbers-index,current-numbers-positions(numbers-index)) = -1
                           MOVE 0 TO memo-at(numbers-index,current-numbers-positions(numbers-index))
                       END-IF
                   END-PERFORM
                   PERFORM shortcircuit-increase-at
               END-IF
               MOVE qmarks-positions(increased-qmark) TO increased-data-index
           END-PERFORM
           DISPLAY "nb-of-combinations = " nb-of-combinations
           ADD nb-of-combinations TO result
           .
       
       increase-additional-pounds.
           PERFORM VARYING qmark-index FROM qmarks-nb BY -1 UNTIL (additional-pounds(qmark-index) = 0) OR (qmark-index = 1)
               CONTINUE
           END-PERFORM
           COMPUTE temp-n-1 = qmarks-nb - qmark-index + 1 *> Number of trailing 1's + 1
           PERFORM VARYING qmark-index FROM qmark-index BY -1 UNTIL (qmark-index = 0) OR (additional-pounds(qmark-index) = 1)
               CONTINUE
           END-PERFORM
           *> qmark-index is the position of the 1
           IF qmark-index = 0
               MOVE 1 TO cannot-increase
           ELSE
               MOVE 0 TO additional-pounds(qmark-index)
               MOVE qmark-index TO increased-qmark
               ADD 1 TO qmark-index
               PERFORM VARYING qmark-index FROM qmark-index BY 1 UNTIL qmark-index > qmarks-nb
                   IF temp-n-1 > 0
                       SUBTRACT 1 FROM temp-n-1
                       MOVE 1 TO additional-pounds(qmark-index)
                   ELSE
                       MOVE 0 TO additional-pounds(qmark-index)
                   END-IF
               END-PERFORM
           END-IF
           .
       
       *> An invalidity was noticed at index `increase-at`: so we can directly increase from the greatest index <= increase-at.
       shortcircuit-increase-at.
           MOVE 1 TO temp-bool-1
           PERFORM VARYING qmark-index FROM increase-at BY -1 UNTIL (additional-pounds(qmark-index) = 1) OR (qmark-index = 0)
               CONTINUE
           END-PERFORM
           MOVE qmark-index TO increased-qmark
           COMPUTE qmark-index-2 = qmark-index + 1
           PERFORM VARYING qmark-index-2 FROM qmark-index-2 BY 1 UNTIL qmark-index-2 > qmarks-nb
               IF additional-pounds(qmark-index-2) = 0
                   MOVE 0 TO temp-bool-1
               END-IF
           END-PERFORM
           IF (qmark-index = 0) OR (temp-bool-1 = 1)
               IF qmark-index = 0
                   MOVE 1 TO cannot-increase
               ELSE
                   PERFORM increase-additional-pounds
               END-IF
           ELSE
               MOVE 0 TO temp-n-1
               PERFORM VARYING qmark-index-2 FROM qmark-index BY 1 UNTIL qmark-index-2 > qmarks-nb
                   IF additional-pounds(qmark-index-2) = 1
                       MOVE 0 TO additional-pounds(qmark-index-2)
                       ADD 1 TO temp-n-1
                   END-IF
               END-PERFORM
               ADD 1 TO qmark-index
               PERFORM VARYING qmark-index FROM qmark-index BY 1 UNTIL temp-n-1 = 0
                   MOVE 1 TO additional-pounds(qmark-index)
                   SUBTRACT 1 FROM temp-n-1
               END-PERFORM 
           END-IF
           .
       
       is-current-data-valid.
           PERFORM VARYING qmark-index FROM increased-qmark BY 1 UNTIL qmark-index > qmarks-nb
               IF additional-pounds(qmark-index) = 1
                   MOVE "#" TO current-data(qmarks-positions(qmark-index):1)
               ELSE
                   MOVE "." TO current-data(qmarks-positions(qmark-index):1)
               END-IF
           END-PERFORM
           MOVE 1 TO data-valid
           MOVE 0 TO temp-n-1
           MOVE 0 TO temp-n-2
           MOVE 1 TO qmark-index
           MOVE 1 TO numbers-index
           MOVE 0 TO increase-at
           PERFORM VARYING data-index FROM 1 BY 1 UNTIL data-index > data-length
               IF (qmarks-positions(qmark-index) = data-index) AND (qmark-index < qmarks-nb)
                   ADD 1 TO qmark-index
               END-IF
               IF current-data(data-index:1) = "#"
                   ADD 1 TO temp-n-1
                   IF temp-n-1 = 1
                       MOVE data-index TO current-numbers-positions(numbers-index)
                       IF (memo-at(numbers-index,data-index) >= 0) AND (NOT data-index = old-numbers-positions(numbers-index))
                           IF memo-at(numbers-index,data-index) = 0
                               MOVE 0 TO data-valid
                           END-IF
                           COMPUTE data-index = data-length + 1 *> exit the loop
                           COMPUTE increase-at = qmark-index - 1
                       END-IF
                       MOVE data-index TO old-numbers-positions(numbers-index)
                   ELSE
                       IF temp-n-1 > current-numbers(numbers-index)
                           MOVE 0 TO data-valid
                           COMPUTE data-index = data-length + 1 *> exit the loop
                           COMPUTE increase-at = qmark-index - 1
                       END-IF
                   END-IF
               ELSE
                   IF temp-n-1 > 0
                       IF NOT (temp-n-1 = current-numbers(numbers-index))
                           MOVE 0 TO data-valid
                           COMPUTE data-index = data-length + 1 *> exit the loop
                           COMPUTE increase-at = qmark-index - 1
                       END-IF
                       ADD 1 TO numbers-index
                       MOVE 0 TO temp-n-1
                   END-IF
               END-IF
           END-PERFORM
           .
       
       parse-line.
           MOVE 1 TO parse-position
           MOVE 0 TO qmarks-nb
           MOVE 0 TO pounds-nb
           MOVE 0 TO numbers-total
           PERFORM UNTIL current-line(parse-position:1) = " "
               MOVE current-line(parse-position:1) TO current-data(parse-position:1)
               IF current-line(parse-position:1) = "?"
                   ADD 1 TO qmarks-nb
                   MOVE parse-position TO qmarks-positions(qmarks-nb)
               END-IF
               IF current-line(parse-position:1) = "#"
                   ADD 1 TO pounds-nb
               END-IF
               ADD 1 TO parse-position
           END-PERFORM
           COMPUTE data-length = parse-position - 1

           ADD 1 TO parse-position
           MOVE 0 TO numbers-length
           PERFORM UNTIL current-line(parse-position:1) = " "
               PERFORM parse-number
               ADD 1 TO numbers-length
               MOVE parsed-number TO current-numbers(numbers-length)
               ADD parsed-number TO numbers-total
               ADD 1 TO parse-position
           END-PERFORM
           
           *> Multiply the data by 5
           MULTIPLY 5 BY pounds-nb
           MULTIPLY 5 BY numbers-total

           MOVE numbers-length TO temp-n-1
           MOVE data-length    TO temp-n-2
           PERFORM 4 TIMES
               MOVE 0 TO numbers-index
               PERFORM temp-n-1 TIMES
                   ADD 1 TO numbers-length
                   ADD 1 TO numbers-index
                   MOVE current-numbers(numbers-index) TO current-numbers(numbers-length)
               END-PERFORM

               ADD 1 TO data-length
               MOVE "?" TO current-data(data-length:1)
               ADD 1 TO qmarks-nb
               MOVE data-length TO qmarks-positions(qmarks-nb)
               MOVE 0 TO data-index
               PERFORM temp-n-2 TIMES
                   ADD 1 TO data-length
                   ADD 1 TO data-index
                   MOVE current-data(data-index:1) TO current-data(data-length:1)
                   IF current-data(data-index:1) = "?"
                       ADD 1 TO qmarks-nb
                       MOVE data-length TO qmarks-positions(qmarks-nb)
                   END-IF
               END-PERFORM
           END-PERFORM
           .
       
       parse-number.
           MOVE 0 TO parsed-number
           MOVE 1 TO temp-bool-1
           PERFORM UNTIL temp-bool-1 = 0
               MOVE current-line(parse-position:1) TO char-as-int
               IF (char-as-int = 0) AND NOT (current-line(parse-position:1) = "0")
                   MOVE 0 TO temp-bool-1
               ELSE
                   COMPUTE parsed-number = 10 * parsed-number + char-as-int
                   ADD 1 TO parse-position 
               END-IF
           END-PERFORM
           .
END PROGRAM AOC-DAY-12.
