IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-12.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day12/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(100).

       WORKING-STORAGE SECTION.
       77 parse-position  USAGE INDEX.
       77 char-as-int     PIC 9.
       77 parsed-number   PIC 9(4) USAGE COMPUTATIONAL.

       77 current-data    PIC X(20).
       77 data-length     USAGE INDEX.
       77 data-index      USAGE INDEX.
       77 current-numbers PIC 99 OCCURS 20 TIMES INDEXED BY numbers-index USAGE COMPUTATIONAL.
       77 numbers-length  USAGE INDEX.
       77 numbers-total   PIC 99 USAGE COMPUTATIONAL.

       77 qmarks-positions PIC 99 OCCURS 20 TIMES INDEXED BY qmark-index USAGE COMPUTATIONAL.
       77 qmarks-nb       PIC 99 USAGE COMPUTATIONAL.
       77 pounds-nb       PIC 99 USAGE COMPUTATIONAL.
       77 additional-pounds PIC 9 OCCURS 20 TIMES USAGE COMPUTATIONAL.
       77 cannot-increase PIC 9.
       77 data-valid      PIC 9.
       77 nb-of-combinations PIC 9(16) USAGE COMPUTATIONAL.

       77 temp-n-1        PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
       77 temp-bool-1     PIC 9.
       77 temp-bool-2     PIC 9.
       77 result          PIC 9(16) USAGE COMPUTATIONAL VALUE 0.
       
       77 temp-display PIC X(100).
       77 end-of-file     PIC 9 VALUE 0.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           PERFORM process-line
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       
       CLOSE file-fd
       
       DISPLAY "result = " result
       
       STOP RUN.

       process-line.
           MOVE 0 TO nb-of-combinations
           PERFORM parse-line
           
           *> expected number of '#' to fill
           COMPUTE temp-n-1 = numbers-total - pounds-nb
           PERFORM VARYING qmark-index FROM 1 BY 1 UNTIL qmark-index > 20
               IF qmark-index <= temp-n-1
                   MOVE 1 TO additional-pounds(qmark-index)
               ELSE
                   MOVE 0 TO additional-pounds(qmark-index)
               END-IF
           END-PERFORM

           MOVE 0 TO cannot-increase
           PERFORM UNTIL cannot-increase = 1
               PERFORM is-current-data-valid

               IF data-valid = 1
                   ADD 1 TO nb-of-combinations
               END-IF
               PERFORM increase-additional-pounds
           END-PERFORM
           ADD nb-of-combinations TO result
           DISPLAY "nb-of-combinations = " nb-of-combinations
           .
       
       increase-additional-pounds.
           PERFORM VARYING qmark-index FROM qmarks-nb BY -1 UNTIL (additional-pounds(qmark-index) = 0) OR (qmark-index = 1)
               CONTINUE
           END-PERFORM
           COMPUTE temp-n-1 = qmarks-nb - qmark-index + 1 *> Number of trailing 1's + 1
           PERFORM VARYING qmark-index FROM qmark-index BY -1 UNTIL (qmark-index = 0) OR (additional-pounds(qmark-index) = 1)
               CONTINUE
           END-PERFORM
           *> qmark-index is the position of the 1
           IF qmark-index = 0
               MOVE 1 TO cannot-increase
           ELSE
               MOVE 0 TO additional-pounds(qmark-index)
               ADD 1 TO qmark-index
               PERFORM VARYING qmark-index FROM qmark-index BY 1 UNTIL qmark-index > qmarks-nb
                   IF temp-n-1 > 0
                       SUBTRACT 1 FROM temp-n-1
                       MOVE 1 TO additional-pounds(qmark-index)
                   ELSE
                       MOVE 0 TO additional-pounds(qmark-index)
                   END-IF
               END-PERFORM
           END-IF
           .
       
       is-current-data-valid.
           PERFORM VARYING qmark-index FROM 1 BY 1 UNTIL qmark-index > qmarks-nb
               IF additional-pounds(qmark-index) = 1
                   MOVE "#" TO current-data(qmarks-positions(qmark-index):1)
               ELSE
                   MOVE "." TO current-data(qmarks-positions(qmark-index):1)
               END-IF
           END-PERFORM
           MOVE 1 TO data-valid
           MOVE 1 TO numbers-index
           MOVE 0 TO temp-n-1
           MOVE 0 TO temp-bool-1
           PERFORM VARYING data-index FROM 1 BY 1 UNTIL data-index > data-length
               IF current-data(data-index:1) = "#"
                   ADD 1 TO temp-n-1
                   MOVE 1 TO temp-bool-1
               ELSE
                   IF temp-bool-1 = 1
                       MOVE 0 TO temp-bool-1
                       IF NOT (temp-n-1 = current-numbers(numbers-index))
                           MOVE 0 TO data-valid
                           COMPUTE data-index = data-length + 1
                       END-IF
                       ADD 1 TO numbers-index
                       MOVE 0 TO temp-n-1
                   END-IF
               END-IF
           END-PERFORM
           IF (numbers-index <= numbers-length) AND NOT (temp-n-1 = current-numbers(numbers-index))
               MOVE 0 TO data-valid
           END-IF
           .
       
       parse-line.
           MOVE 1 TO parse-position
           MOVE 0 TO qmarks-nb
           MOVE 0 TO pounds-nb
           MOVE 0 TO numbers-total
           PERFORM UNTIL current-line(parse-position:1) = " "
               MOVE current-line(parse-position:1) TO current-data(parse-position:1)
               IF current-line(parse-position:1) = "?"
                   ADD 1 TO qmarks-nb
                   MOVE parse-position TO qmarks-positions(qmarks-nb)
               END-IF
               IF current-line(parse-position:1) = "#"
                   ADD 1 TO pounds-nb
               END-IF
               ADD 1 TO parse-position
           END-PERFORM
           COMPUTE data-length = parse-position - 1

           ADD 1 TO parse-position
           MOVE 1 TO numbers-index
           PERFORM UNTIL current-line(parse-position:1) = " "
               PERFORM parse-number
               MOVE parsed-number TO current-numbers(numbers-index)
               ADD parsed-number TO numbers-total
               ADD 1 TO numbers-index
               ADD 1 TO parse-position
           END-PERFORM
           COMPUTE numbers-length = numbers-index - 1
           .
       
       parse-number.
           MOVE 0 TO parsed-number
           MOVE 1 TO temp-bool-1
           PERFORM UNTIL temp-bool-1 = 0
               MOVE current-line(parse-position:1) TO char-as-int
               IF (char-as-int = 0) AND NOT (current-line(parse-position:1) = "0")
                   MOVE 0 TO temp-bool-1
               ELSE
                   COMPUTE parsed-number = 10 * parsed-number + char-as-int
                   ADD 1 TO parse-position 
               END-IF
           END-PERFORM
           .
END PROGRAM AOC-DAY-12.
