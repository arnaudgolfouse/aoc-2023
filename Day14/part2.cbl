IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-14.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day14/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       REPLACE ==LineSize== BY ==100==.
       FD file-fd.
       01 current-line PIC X(LineSize).

       WORKING-STORAGE SECTION.
       01 platform OCCURS LineSize TIMES INDEXED BY platform-line-idx.
           02 platform-line PIC X(LineSize).
       77 platform-line-idx-2 USAGE INDEX.
       77 platform-col-idx    USAGE INDEX.
       77 platform-col-idx-2  USAGE INDEX.
       77 free-position       USAGE INDEX.
       77 total-load          PIC 9(16) USAGE COMPUTATIONAL.
       77 weight              PIC 9(16) USAGE COMPUTATIONAL.

       *> cycle detection (not good, but eh)
       77 loads PIC 9(16) OCCURS 10 TIMES USAGE COMPUTATIONAL.
       77 loads-101-to-110 PIC 9(16) OCCURS 10 TIMES USAGE COMPUTATIONAL.
       77 i1 USAGE INDEX.
       77 i2 USAGE INDEX.
       77 is-cycle PIC 9.
       77 cycle-length USAGE INDEX.
       77 cycles-left USAGE INDEX.
       
       77 temp-char PIC X.
PROCEDURE DIVISION.
       *> IDEAS:
       *> - Keep a 'stable after one cycle' array
       *>   If a rolling stone only bumps against stable objects AND stays at the same place, then it's position (at the start/end of the cycle) is marked as stable.
       *>   Once all positions are stable, finish !
       OPEN INPUT file-fd
       PERFORM VARYING platform-line-idx FROM 1 BY 1 UNTIL platform-line-idx > LineSize
           READ file-fd
           MOVE current-line TO platform-line(platform-line-idx)
       END-PERFORM
       CLOSE file-fd
       
       *> 'warmup'
       PERFORM 100 TIMES
           PERFORM cycle
       END-PERFORM
       
       *> This will be the pattern we want to detect
       PERFORM VARYING i1 FROM 1 BY 1 UNTIL i1 > 10
           PERFORM cycle
           PERFORM compute-total-load
           MOVE total-load TO loads-101-to-110(i1)
           MOVE total-load TO loads(i1)
       END-PERFORM
       
       MOVE 100 TO i1
       MOVE 0 TO cycle-length
       PERFORM UNTIL NOT cycle-length = 0
           PERFORM cycle
           PERFORM compute-total-load
           ADD 1 TO i1
           MOVE 1 TO is-cycle
           PERFORM VARYING i2 FROM 1 BY 1 UNTIL i2 > 9
               MOVE loads(i2 + 1) TO loads(i2)
               IF NOT loads(i2) = loads-101-to-110(i2)
                   MOVE 0 TO is-cycle
               END-IF
           END-PERFORM
           MOVE total-load TO loads(10)
           IF NOT total-load = loads-101-to-110(10)
               MOVE 0 TO is-cycle
           END-IF

           IF is-cycle = 1
               COMPUTE cycle-length = i1 - 100
           END-IF
       END-PERFORM
       
       DISPLAY "cycle-length = " cycle-length

       COMPUTE cycles-left = FUNCTION MOD (1000000000 - 10 - i1, cycle-length)

       DISPLAY cycles-left " cycles left"

       PERFORM cycles-left TIMES
           PERFORM cycle
       END-PERFORM
       PERFORM compute-total-load
       DISPLAY "load = " total-load
              
       STOP RUN.

       compute-total-load.
           MOVE 0 TO total-load
           MOVE LineSize TO weight
           PERFORM VARYING platform-line-idx FROM 1 BY 1 UNTIL platform-line-idx > LineSize
               PERFORM VARYING platform-col-idx FROM 1 BY 1 UNTIL platform-col-idx > LineSize
                   IF platform-line(platform-line-idx) (platform-col-idx:1) = "O"
                       COMPUTE total-load = total-load + weight
                   END-IF
               END-PERFORM
               SUBTRACT 1 FROM weight
           END-PERFORM
           .

       cycle.
           PERFORM tilt-north
           PERFORM mirror
           PERFORM tilt-north
           PERFORM mirror2
           PERFORM tilt-north
           PERFORM mirror
           PERFORM tilt-north
           PERFORM mirror2
           .

       tilt-north.
           PERFORM VARYING platform-col-idx FROM 1 BY 1 UNTIL platform-col-idx > LineSize
               MOVE 0 TO free-position
               PERFORM VARYING platform-line-idx FROM 1 BY 1 UNTIL platform-line-idx > LineSize
                   EVALUATE platform-line(platform-line-idx) (platform-col-idx:1)
                    WHEN "#" MOVE 0 TO free-position
                    WHEN "O"
                       IF NOT (free-position = 0)
                           MOVE "O" TO platform-line(free-position) (platform-col-idx:1)
                           MOVE "." TO platform-line(platform-line-idx) (platform-col-idx:1)
                           ADD 1 TO free-position
                       END-IF
                    WHEN "." 
                       IF free-position = 0
                           MOVE platform-line-idx TO free-position
                       END-IF
                   END-EVALUATE
               END-PERFORM
           END-PERFORM
           .
       
       *> Mirror along the top-left axis: \
       mirror.
           PERFORM VARYING platform-line-idx FROM 1 BY 1 UNTIL platform-line-idx > LineSize
               COMPUTE platform-col-idx = platform-line-idx + 1
               PERFORM VARYING platform-col-idx FROM platform-col-idx BY 1 UNTIL platform-col-idx > LineSize
                   MOVE platform-line(platform-line-idx) (platform-col-idx:1) TO temp-char
                   MOVE platform-line(platform-col-idx) (platform-line-idx:1) TO platform-line(platform-line-idx) (platform-col-idx:1)
                   MOVE temp-char TO platform-line(platform-col-idx) (platform-line-idx:1)
               END-PERFORM
           END-PERFORM
           .
       
       *> Mirror along the top-right axis: /
       mirror2.
           PERFORM VARYING platform-line-idx FROM 1 BY 1 UNTIL platform-line-idx > LineSize
               COMPUTE platform-col-idx-2 = LineSize - platform-line-idx + 1
               PERFORM VARYING platform-col-idx FROM 1 BY 1 UNTIL platform-col-idx > LineSize - platform-line-idx
                   COMPUTE platform-line-idx-2 = LineSize - platform-col-idx + 1
                   MOVE platform-line(platform-line-idx) (platform-col-idx:1) TO temp-char
                   MOVE platform-line(platform-line-idx-2) (platform-col-idx-2:1) TO platform-line(platform-line-idx) (platform-col-idx:1)
                   MOVE temp-char TO platform-line(platform-line-idx-2) (platform-col-idx-2:1)
               END-PERFORM
           END-PERFORM
           .
END PROGRAM AOC-DAY-14.
