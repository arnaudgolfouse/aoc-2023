IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-20.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day20/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(30).

       WORKING-STORAGE SECTION.
       REPLACE ==ModulesNb== BY ==58== ;
               ==PulsesNb== BY ==10000==.
       77 parse-index INDEX.

       01 FILLER OCCURS ModulesNb TIMES INDEXED BY module-index.
           02 mod-kind PIC X.
           02 mod-name PIC X(2).
           *> For flip-flop modules
           02 on-off   PIC 9 COMP.
           *> 0-terminated
           02 destination-modules PIC 99 COMP OCCURS 7 TIMES INDEXED BY dest-index VALUES 0.
           02 destination-rx PIC 9 COMP VALUE 0.
           *> For conjuction modules
           *> 0-terminated
           02 FILLER OCCURS 16 TIMES INDEXED BY orig-index.
               03 origin-module PIC 99 COMP VALUE 0.
               03 last-input PIC 9 COMP VALUE 0.
       
       01 FILLER OCCURS PulsesNb TIMES.
           02 pulses-from PIC 99 COMP.
           02 pulses-to   PIC 99 COMP.
           02 is-high     PIC 9  COMP.
       
       77 pulses-front-idx INDEX VALUE 1.
       77 pulses-back-idx  INDEX VALUE 1.
       77 module PIC 99 COMP.
       77 high   PIC 9  COMP.
       77 send-module PIC 99 COMP.
       77 send-high   PIC 9 COMP.
       77 from-module PIC 99 COMP.

       77 module-index-2 PIC 99 COMP.
       77 start-module   PIC 99 COMP.

       77 temp-bool PIC 9.

       77 total-low-pulses PIC 9(10) COMP VALUE 0.
       77 total-high-pulses PIC 9(10) COMP VALUE 0.
       77 result PIC 9(10) COMP VALUE 0.
PROCEDURE DIVISION.
       *> First pass: get names
       OPEN INPUT file-fd
       
       PERFORM VARYING module-index FROM 1 BY 1 UNTIL module-index > ModulesNb
           READ file-fd
           IF current-line(1:1) = "b"
               MOVE " " TO mod-kind(module-index)
           ELSE
               MOVE current-line(1:1) TO mod-kind(module-index)
           END-IF
           MOVE current-line(2:2) TO mod-name(module-index)
           IF current-line(1:9) = "broadcast"
               MOVE module-index TO start-module
           END-IF
       END-PERFORM
       CLOSE file-fd

       *> Second pass: use indices
       OPEN INPUT file-fd
       PERFORM VARYING module-index FROM 1 BY 1 UNTIL module-index > ModulesNb
           READ file-fd
           PERFORM VARYING parse-index FROM 1 BY 1 UNTIL current-line(parse-index:2) = "->"
               CONTINUE
           END-PERFORM
           ADD 3 TO parse-index
           MOVE 0 TO temp-bool
           MOVE 1 TO dest-index
           PERFORM UNTIL temp-bool = 1
               PERFORM VARYING module-index-2 FROM 1 BY 1 UNTIL module-index-2 > ModulesNb
                   IF mod-name(module-index-2) = current-line(parse-index:2)
                       MOVE module-index-2 TO destination-modules(module-index,dest-index)
                   END-IF
               END-PERFORM
               ADD 2 TO parse-index
               IF NOT current-line(parse-index:1) = ","
                   MOVE 1 TO temp-bool
               END-IF
               IF destination-modules(module-index,dest-index) = 0
                   *> rx module, does not appear in the list
                   MOVE 1 TO destination-rx(module-index)
               ELSE
                   *> Add to the list of origin modules !
                   MOVE destination-modules(module-index,dest-index) TO module-index-2
                   PERFORM VARYING orig-index FROM 1 BY 1 UNTIL origin-module(module-index-2,orig-index) = 0
                       CONTINUE
                   END-PERFORM
                   MOVE module-index TO origin-module(module-index-2,orig-index)
                   ADD 1 TO dest-index
               END-IF
               ADD 2 TO parse-index
           END-PERFORM
           MOVE 0 TO destination-modules(module-index,dest-index + 1)
       END-PERFORM
       CLOSE file-fd
       
       PERFORM 1000 TIMES
           MOVE start-module TO module
           MOVE start-module TO send-module
           MOVE 0 TO send-high
           PERFORM push-pulse
           PERFORM UNTIL pulses-back-idx = pulses-front-idx
               PERFORM process-pulse
           END-PERFORM
       END-PERFORM
       
       COMPUTE result = total-low-pulses * total-high-pulses
       DISPLAY "result = " result " (" total-low-pulses " low, " total-high-pulses " high)"
       
       STOP RUN.

       process-pulse.
           PERFORM pop-pulse
           EVALUATE mod-kind(module)
            WHEN " "
               MOVE 0 TO send-high
               PERFORM VARYING dest-index FROM 1 BY 1 UNTIL destination-modules(module,dest-index) = 0
                   MOVE destination-modules(module,dest-index) TO send-module
                   PERFORM push-pulse
               END-PERFORM
               MOVE 99 TO send-module
               IF destination-rx(module) = 1
                   PERFORM push-pulse
               END-IF
            WHEN "%"
               IF high = 0
                   COMPUTE on-off(module) = 1 - on-off(module)
                   MOVE on-off(module) TO send-high
                   PERFORM VARYING dest-index FROM 1 BY 1 UNTIL destination-modules(module,dest-index) = 0
                       MOVE destination-modules(module,dest-index) TO send-module
                       PERFORM push-pulse
                   END-PERFORM
                   MOVE 99 TO send-module
                   IF destination-rx(module) = 1
                       PERFORM push-pulse
                   END-IF
               END-IF
            WHEN "&"
               PERFORM VARYING orig-index FROM 1 BY 1 UNTIL origin-module(module,orig-index) = from-module
                   CONTINUE
               END-PERFORM
               MOVE high TO last-input(module,orig-index)
               MOVE 0 TO send-high
               PERFORM VARYING orig-index FROM 1 BY 1 UNTIL origin-module(module,orig-index) = 0
                   IF last-input(module,orig-index) = 0
                       MOVE 1 TO send-high
                   END-IF
               END-PERFORM
               PERFORM VARYING dest-index FROM 1 BY 1 UNTIL destination-modules(module,dest-index) = 0
                   MOVE destination-modules(module,dest-index) TO send-module
                   PERFORM push-pulse
               END-PERFORM
               MOVE 99 TO send-module
               IF destination-rx(module) = 1
                   PERFORM push-pulse
               END-IF
           END-EVALUATE
           .

       push-pulse.
           IF send-high = 0
               ADD 1 TO total-low-pulses
           ELSE
               ADD 1 TO total-high-pulses
           END-IF
           IF NOT send-module = 99
               MOVE send-module TO pulses-to(pulses-back-idx)
               MOVE send-high TO is-high(pulses-back-idx)
               MOVE module TO pulses-from(pulses-back-idx)
               ADD 1 TO pulses-back-idx
               IF pulses-back-idx > PulsesNb
                   MOVE 1 TO pulses-back-idx
               END-IF
           END-IF
           .
       
       pop-pulse.
           MOVE pulses-to(pulses-front-idx)   TO module
           MOVE is-high(pulses-front-idx)     TO high
           MOVE pulses-from(pulses-front-idx) TO from-module
            ADD 1 TO pulses-front-idx
           IF pulses-front-idx > PulsesNb
               MOVE 1 TO pulses-front-idx
           END-IF
           .
END PROGRAM AOC-DAY-20.
