IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-17.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day17/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       REPLACE ==MapSize== BY ==141== ;
               ==StackSize== BY ==80000==.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(MapSize).

       WORKING-STORAGE SECTION.
       77 map PIC X(MapSize) OCCURS MapSize TIMES INDEXED BY map-line.
       77 map-col       USAGE INDEX.

       01 FILLER OCCURS MapSize TIMES.
           02 FILLER OCCURS MapSize TIMES.
               *> 1: incoming direction is horizontal
               *> 2: incoming direction is vertical
               03 FILLER OCCURS 2 TIMES INDEXED BY direction-index.
                   04 distance           PIC 9(16) COMPUTATIONAL VALUE 9999999999999999.
                   04 previous-line      PIC 999 COMPUTATIONAL.
                   04 previous-col       PIC 999 COMPUTATIONAL.
                   04 previous-direction PIC 9 COMPUTATIONAL.
                   04 index-in-priority  USAGE INDEX.

                   04 neighbors OCCURS 7 TIMES INDEXED BY neighbor-index.
                       *> 0 => the neighbors stop here
                       *> 1 => vertical
                       *> 2 => horizontal
                       05 neighb-direction PIC 9 COMPUTATIONAL VALUE 0.
                       05 neighb-line      PIC 999 COMPUTATIONAL.
                       05 neighb-col       PIC 999 COMPUTATIONAL.
                       05 neighb-distance  PIC 99 COMPUTATIONAL.
       77 next-map-line  USAGE INDEX.
       77 next-map-col   USAGE INDEX.
       77 next-direction USAGE INDEX.
       77 next-distance  PIC 9(16) COMPUTATIONAL VALUE 9999999999999999.

       *> smallest one at the top
       01 priority-stack OCCURS StackSize TIMES INDEXED BY prio-index.
           02 prio-direction PIC 9     COMPUTATIONAL.
           02 prio-line      PIC 999   COMPUTATIONAL.
           02 prio-col       PIC 999   COMPUTATIONAL.
        *>    02 prio-distance  PIC 9(16) COMPUTATIONAL.
       77 prio-length USAGE INDEX VALUE 0.
       
       77 temp-n-1  PIC 9(16) COMPUTATIONAL.
       77 temp-n-2  PIC 9(16) COMPUTATIONAL.
       77 temp-n-3  PIC 9(16) COMPUTATIONAL.
       77 char-as-int PIC 9.
       77 result    PIC 9(16) COMPUTATIONAL.
PROCEDURE DIVISION.
       OPEN INPUT file-fd
       PERFORM VARYING map-line FROM 1 BY 1 UNTIL map-line > MapSize
           READ file-fd
           MOVE current-line TO map(map-line)
       END-PERFORM
       CLOSE file-fd
       
       PERFORM fill-neighbors-map
       
       *> Initialize Dijkstra
       MOVE 0 TO distance(1,1,1)
       MOVE 1 TO previous-line(1,1,1)
       MOVE 1 TO previous-col(1,1,1)
       MOVE 1 TO previous-direction(1,1,1)
       MOVE 0 TO distance(1,1,2)
       MOVE 1 TO previous-line(1,1,2)
       MOVE 1 TO previous-col(1,1,2)
       MOVE 2 TO previous-direction(1,1,2)

       MOVE 2 TO prio-length
       MOVE 1 TO prio-line(1)
       MOVE 1 TO prio-col(1)
       MOVE 1 TO prio-direction(1)
       MOVE 1 TO index-in-priority(1,1,1)
       MOVE 1 TO prio-line(2)
       MOVE 1 TO prio-col(2)
       MOVE 2 TO prio-direction(2)
       MOVE 2 TO index-in-priority(1,1,2)

       PERFORM UNTIL prio-length = 0
           PERFORM find-next-path
       END-PERFORM

       MOVE distance(MapSize,MapSize,1) TO result
       IF distance(MapSize,MapSize,2) < result
           MOVE distance(MapSize,MapSize,2) TO result
       END-IF

       DISPLAY "result = " result
       
       STOP RUN.
       
       *> Perform Dijkstra's algorithm (with a bad priority queue :) )
       find-next-path.
           MOVE prio-line(prio-length) TO map-line
           MOVE prio-col(prio-length) TO map-col
           MOVE prio-direction(prio-length) TO direction-index
           MOVE -1 TO index-in-priority(map-line,map-col,direction-index)
           SUBTRACT 1 FROM prio-length
           *> Update neighbors
           PERFORM VARYING neighbor-index FROM 1 BY 1 UNTIL neighb-direction(map-line,map-col,direction-index,neighbor-index) = 0
               MOVE neighb-line(map-line,map-col,direction-index,neighbor-index) TO next-map-line
               MOVE neighb-col(map-line,map-col,direction-index,neighbor-index) TO next-map-col
               MOVE neighb-direction(map-line,map-col,direction-index,neighbor-index) TO next-direction
               MOVE distance(map-line,map-col,direction-index) TO next-distance
               ADD neighb-distance(map-line,map-col,direction-index,neighbor-index) TO next-distance
               IF next-distance < distance(next-map-line,next-map-col,next-direction)
                   MOVE next-distance TO distance(next-map-line,next-map-col,next-direction)
                   MOVE map-line TO previous-line(next-map-line,next-map-col,next-direction)
                   MOVE map-col TO previous-col(next-map-line,next-map-col,next-direction)
                   PERFORM insert-in-priority-stack
               END-IF
           END-PERFORM
           .
       
       insert-in-priority-stack.
           MOVE index-in-priority(next-map-line,next-map-col,next-direction) TO prio-index
           IF prio-index = -1
               ADD 1 TO prio-length
               MOVE next-map-line  TO prio-line(prio-length)
               MOVE next-map-col   TO prio-col(prio-length)
               MOVE next-direction TO prio-direction(prio-length)
               MOVE prio-length TO prio-index
               PERFORM UNTIL (prio-index = 1)
                   IF next-distance < distance(prio-line(prio-index - 1),prio-col(prio-index - 1),prio-direction(prio-index - 1))
                       EXIT PERFORM
                   ELSE
                       PERFORM swap-at-prio-index
                       ADD 1 TO index-in-priority(prio-line(prio-index),prio-col(prio-index),prio-direction(prio-index))
                       SUBTRACT 1 FROM prio-index
                   END-IF
               END-PERFORM
           ELSE
               ADD 1 TO prio-index
               PERFORM UNTIL prio-index > prio-length
                   IF next-distance >= distance(prio-line(prio-index),prio-col(prio-index),prio-direction(prio-index))
                       EXIT PERFORM
                   ELSE
                       SUBTRACT 1 FROM index-in-priority(prio-line(prio-index),prio-col(prio-index),prio-direction(prio-index))
                       PERFORM swap-at-prio-index
                       ADD 1 TO prio-index
                   END-IF
               END-PERFORM
               SUBTRACT 1 FROM prio-index
           END-IF
           MOVE prio-index TO index-in-priority(next-map-line,next-map-col,next-direction)
           .
       
       *> Swap (prio-index - 1) and prio-index
       swap-at-prio-index.
           MOVE prio-line(prio-index) TO temp-n-1
           MOVE prio-col(prio-index) TO temp-n-2
           MOVE prio-direction(prio-index) TO temp-n-3
           MOVE prio-line(prio-index - 1) TO prio-line(prio-index)
           MOVE prio-col(prio-index - 1) TO prio-col(prio-index)
           MOVE prio-direction(prio-index - 1) TO prio-direction(prio-index)
           MOVE temp-n-1 TO prio-line(prio-index - 1)
           MOVE temp-n-2 TO prio-col(prio-index - 1)
           MOVE temp-n-3 TO prio-direction(prio-index - 1)
           .
       
       fill-neighbors-map.
           PERFORM VARYING map-line FROM 1 BY 1 UNTIL map-line > MapSize
               PERFORM VARYING map-col FROM 1 BY 1 UNTIL map-col > MapSize
                   *> Coming from the right/left, exiting to the top/bottom
                   MOVE 1 TO direction-index
                   MOVE 1 TO neighbor-index
                   MOVE -1 TO index-in-priority(map-line,map-col,direction-index)
                   MOVE 0 TO temp-n-2 MOVE 0 TO temp-n-3
                   PERFORM VARYING temp-n-1 FROM 1 BY 1 UNTIL temp-n-1 > 3
                       IF map-line + temp-n-1 <= MapSize
                           *> Bottom
                           MOVE map(map-line + temp-n-1) (map-col:1) TO char-as-int
                           ADD char-as-int TO temp-n-2
                           MOVE 2 TO neighb-direction(map-line,map-col,direction-index,neighbor-index)
                           COMPUTE neighb-line(map-line,map-col,direction-index,neighbor-index) = map-line + temp-n-1
                           MOVE map-col TO neighb-col(map-line,map-col,direction-index,neighbor-index)
                           MOVE temp-n-2 TO neighb-distance(map-line,map-col,direction-index,neighbor-index)
                           ADD 1 TO neighbor-index
                       END-IF
                       IF map-line - temp-n-1 > 0
                           *> Top
                           MOVE map(map-line - temp-n-1) (map-col:1) TO char-as-int
                           ADD char-as-int TO temp-n-3
                           MOVE 2 TO neighb-direction(map-line,map-col,direction-index,neighbor-index)
                           COMPUTE neighb-line(map-line,map-col,direction-index,neighbor-index) = map-line - temp-n-1
                           MOVE map-col TO neighb-col(map-line,map-col,direction-index,neighbor-index)
                           MOVE temp-n-3 TO neighb-distance(map-line,map-col,direction-index,neighbor-index)
                           ADD 1 TO neighbor-index
                       END-IF
                   END-PERFORM

                   *> Coming from the top/bottom, exiting to the right/left
                   MOVE 2 TO direction-index
                   MOVE 1 TO neighbor-index
                   MOVE -1 TO index-in-priority(map-line,map-col,direction-index)
                   MOVE 0 TO temp-n-2 MOVE 0 TO temp-n-3
                   PERFORM VARYING temp-n-1 FROM 1 BY 1 UNTIL temp-n-1 > 3
                       IF map-col + temp-n-1 <= MapSize
                           *> Right
                           MOVE map(map-line) (map-col + temp-n-1:1) TO char-as-int
                           ADD char-as-int TO temp-n-2
                           MOVE 1 TO neighb-direction(map-line,map-col,direction-index,neighbor-index)
                           MOVE map-line TO neighb-line(map-line,map-col,direction-index,neighbor-index)
                           COMPUTE neighb-col(map-line,map-col,direction-index,neighbor-index) = map-col + temp-n-1
                           MOVE temp-n-2 TO neighb-distance(map-line,map-col,direction-index,neighbor-index)
                           ADD 1 TO neighbor-index
                       END-IF
                       IF map-col - temp-n-1 > 0
                           *> Left
                           MOVE map(map-line) (map-col - temp-n-1:1) TO char-as-int
                           ADD char-as-int TO temp-n-3
                           MOVE 1 TO neighb-direction(map-line,map-col,direction-index,neighbor-index)
                           MOVE map-line TO neighb-line(map-line,map-col,direction-index,neighbor-index)
                           COMPUTE neighb-col(map-line,map-col,direction-index,neighbor-index) = map-col - temp-n-1
                           MOVE temp-n-3 TO neighb-distance(map-line,map-col,direction-index,neighbor-index)
                           ADD 1 TO neighbor-index
                       END-IF
                   END-PERFORM
               END-PERFORM
           END-PERFORM
           .
END PROGRAM AOC-DAY-17.
