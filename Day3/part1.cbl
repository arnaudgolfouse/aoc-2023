IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-3.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day3/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 next-line PIC X(200).

       WORKING-STORAGE SECTION.
       77 end-of-file PIC Z.
       77 current-line PIC X(200).
       77 previous-line PIC X(200).

       77 parse-position PIC 999.
       77 number-start-position PIC S9(3).
       77 number-length PIC 999.
       77 parsed-number PIC 9(5).
       77 is-part-number PIC Z.
       77 char-to-check PIC X.
       77 search-part-buffer PIC X(7).
       77 search-buffer-position PIC 9.
       77 total PIC 9(7) VALUE ZEROES.

       77 char-as-int PIC 9.
       77 stop-bool PIC Z.

PROCEDURE DIVISION.
       MOVE ALL "." TO current-line
       MOVE ALL "." TO previous-line
       MOVE ALL "." TO next-line
       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       PERFORM UNTIL end-of-file = 1
           PERFORM process-current-line
           
           MOVE current-line TO previous-line
           MOVE next-line TO current-line
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM

       PERFORM process-current-line

       DISPLAY "TOTAL = " total
       
       CLOSE file-fd
       STOP RUN.

       process-current-line.
           MOVE 1 TO parse-position
           PERFORM UNTIL parse-position > 200
               PERFORM find-next-number
               MOVE 0 TO is-part-number
               IF NOT (number-start-position = -1)
                   IF number-start-position > 1
                       *> left char
                       SUBTRACT 1 FROM number-start-position
                       MOVE current-line(number-start-position:1) TO char-to-check
                       PERFORM check-char
                   END-IF
               ELSE
                   MOVE 201 to parse-position
               END-IF

               IF NOT (parse-position > 200)
                   *> right char
                   SUBTRACT 1 FROM parse-position
                   MOVE current-line(parse-position:1) TO char-to-check
                   PERFORM check-char
                   ADD 1 TO parse-position
    
                   ADD 2 TO number-length
    
                   *> line above
                   MOVE ALL "." TO search-part-buffer
                   MOVE previous-line(number-start-position:number-length) TO search-part-buffer
                   MOVE 1 TO search-buffer-position
                   PERFORM number-length TIMES
                       MOVE search-part-buffer(search-buffer-position:1) TO char-to-check
                       PERFORM check-char
                       ADD 1 TO search-buffer-position
                   END-PERFORM
    
                   *> line below
                   MOVE ALL "." TO search-part-buffer
                   MOVE next-line(number-start-position:number-length) TO search-part-buffer
                   MOVE 1 TO search-buffer-position
                   PERFORM number-length TIMES
                       MOVE search-part-buffer(search-buffer-position:1) TO char-to-check
                       PERFORM check-char
                       ADD 1 TO search-buffer-position
                   END-PERFORM
    
                   IF is-part-number = 1
                       DISPLAY parsed-number " is a part number"
                       ADD parsed-number TO total
                   END-IF
               END-IF
           END-PERFORM.
       
       *> Moves `parse-position` until the next number on `current-line` is found.
       *> 
       *> If no number is found, `number-start-position` contains -1
       *> Else, 
       *> - `number-start-position` is the indice of the start of the number
       *> - `number-length` is the length of the number
       *> - `parsed-number` is the value of the number
       *> - `parse-position` is positioned *two characters* after the end of the number:
       *>    234..
       *>        ^ here
       find-next-number.
           MOVE 0 TO stop-bool
           MOVE -1 TO number-start-position
           MOVE 0 TO number-length
           MOVE 0 TO parsed-number
           PERFORM UNTIL stop-bool = 1
               MOVE current-line(parse-position:1) TO char-as-int
               IF (current-line(parse-position:1) = "0") OR NOT (char-as-int = 0)
                   MULTIPLY 10 BY parsed-number
                   ADD char-as-int TO parsed-number
                   ADD 1 TO number-length
                   IF number-start-position = -1
                       *> This is the first digit
                       MOVE parse-position TO number-start-position
                   END-IF
               ELSE
                   IF NOT (number-start-position = -1)
                       *> This was the last digit
                       MOVE 1 TO stop-bool
                   END-IF
               END-IF
               ADD 1 TO parse-position
               IF parse-position > 200
                   MOVE 1 TO stop-bool
               END-IF
           END-PERFORM.
       
       check-char.
           MOVE char-to-check TO char-as-int
           IF (char-as-int = 0) AND NOT (char-to-check = "0") AND NOT (char-to-check = ".") AND NOT (char-to-check = " ")
               MOVE 1 TO is-part-number
           END-IF.
END PROGRAM AOC-DAY-3.
