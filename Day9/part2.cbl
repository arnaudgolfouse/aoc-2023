IDENTIFICATION DIVISION.
       PROGRAM-ID.    AOC-DAY-9.

ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT file-fd ASSIGN TO "Day9/input.txt"
            ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
       FILE SECTION.
       FD file-fd.
       01 current-line PIC X(300).

       WORKING-STORAGE SECTION.
       REPLACE ==NumbersOnLine== BY ==21==.
       77 numbers-array       PIC S9(16) OCCURS NumbersOnLine TIMES INDEXED BY number-index USAGE COMPUTATIONAL.
       77 pascal-triangle     PIC S9(16) OCCURS NumbersOnLine TIMES USAGE COMPUTATIONAL.
       77 extrapolated-number PIC S9(16) USAGE COMPUTATIONAL.
       77 total               PIC S9(16) USAGE COMPUTATIONAL.

       77 end-of-file PIC 9.

       77 temp-number PIC S9(16) USAGE COMPUTATIONAL.
       77 temp-bool   PIC 9      USAGE COMPUTATIONAL.
PROCEDURE DIVISION.
       PERFORM fill-pascal-triangle.

       OPEN INPUT file-fd
       READ file-fd
           AT END MOVE 1 TO end-of-file
       END-READ

       MOVE 0 TO total
       
       PERFORM UNTIL end-of-file = 1
           PERFORM process-line
           ADD extrapolated-number TO total
           READ file-fd
               AT END MOVE 1 TO end-of-file
           END-READ
       END-PERFORM
       
       CLOSE file-fd

       DISPLAY "sum of extrapolated values = " total
       
       STOP RUN.

       fill-pascal-triangle.
           MOVE 1 TO pascal-triangle(1)
           PERFORM VARYING number-index FROM 2 BY 1 UNTIL number-index > NumbersOnLine
               MOVE 0 TO pascal-triangle(number-index)
           END-PERFORM
           COMPUTE temp-number = NumbersOnLine - 1
           PERFORM temp-number TIMES
               MOVE 1 TO temp-number
               PERFORM VARYING number-index FROM 1 BY 1 UNTIL number-index > NumbersOnLine
                   COMPUTE pascal-triangle(number-index) = pascal-triangle(number-index) + temp-number
                   COMPUTE temp-number = pascal-triangle(number-index) - temp-number
               END-PERFORM
           END-PERFORM
           PERFORM VARYING number-index FROM 1 BY 1 UNTIL number-index > NumbersOnLine
               COMPUTE temp-bool = FUNCTION MOD (number-index, 2)
               IF temp-bool = 0
                    COMPUTE pascal-triangle(number-index) = -pascal-triangle(number-index)
               END-IF
           END-PERFORM
           .
       
       *> Compute the extrapolated number
       process-line.
           UNSTRING current-line DELIMITED BY Space
               INTO numbers-array(01)
                    numbers-array(02)
                    numbers-array(03)
                    numbers-array(04)
                    numbers-array(05)
                    numbers-array(06)
                    numbers-array(07)
                    numbers-array(08)
                    numbers-array(09)
                    numbers-array(10)
                    numbers-array(11)
                    numbers-array(12)
                    numbers-array(13)
                    numbers-array(14)
                    numbers-array(15)
                    numbers-array(16)
                    numbers-array(17)
                    numbers-array(18)
                    numbers-array(19)
                    numbers-array(20)
                    numbers-array(21)
           END-UNSTRING

           MOVE 0 TO extrapolated-number
           PERFORM VARYING number-index FROM 1 BY 1 UNTIL number-index > NumbersOnLine
               COMPUTE extrapolated-number = extrapolated-number + numbers-array(number-index) * pascal-triangle(number-index)
           END-PERFORM
           .
END PROGRAM AOC-DAY-9.
